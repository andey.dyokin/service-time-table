# ServiceTimeTable backend application

### To Run the application use the following command:
>mvn:clean spring-boot:run

### If using an Intelij Idea just run main method of this class:
[ServiceTimeTableApplication.kt](src/main/kotlin/com/github/belbli/ServiceTimeTableApplication.kt)

### Swagger-ui link:
[http://localhost:8080/swagger-ui/](http://localhost:8080/swagger-ui/)

### Feature plan location:
[Feature plan](TODO.md)