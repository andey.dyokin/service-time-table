##Emoji notation
- 🔨 == In Progress
- 🚩 == Next Planned Task
- 📊 == Need Analysis and more knowledge
- 🙌 == Dream Feature 
- ❓ == Proposal feature. Use it if you have an idea of a feature. Just add
the proposal or link to the proposals and mark it with this symbol.
- 👍 == Submitted proposal
- ❌ == Disinclined Proposal

###Feature Plan:

- [ ] Implement all services 🔨
    - [ ] Create a unit tests for the services
    - [ ] Create an integration tests for the services
- [ ] Implement all controllers 🚩
    - [ ] Create a unit tests for the controllers
    - [ ] Create an integration tests for the controllers
- [ ] Add review/comments to organizations/specialists to create a rating for organizations/specialists
- [ ] Add chats with specialists 📊 🙌
- [ ] Add online meetings/consultations 📊 🙌
- [ ] Add payment method for the services 📊 🙌

###Proposals:
- [x] The example of the submitted proposal. 👍
- [ ] The example of the disinclined proposal.❌