package com.github.belbli.util

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.stereotype.Component
import java.util.stream.Collectors
import javax.annotation.PostConstruct

@Component
class CustomMapper : ObjectMapper() {
    @PostConstruct
    fun setup() {
        configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    }

    fun <D, T> mapToList(source: List<T>, target: Class<D>): List<D> {
        return source.stream()
                .map { el: T -> target.cast(convertValue(el, target)) }
                .collect(Collectors.toList())
    }
}