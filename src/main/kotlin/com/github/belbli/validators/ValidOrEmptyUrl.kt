package com.github.belbli.validators

import org.apache.commons.validator.routines.UrlValidator
import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.Payload
import kotlin.reflect.KClass

@Target(AnnotationTarget.FIELD)
@MustBeDocumented
@Constraint(validatedBy = [ValidOrEmptyUrlValidator::class])
annotation class ValidOrEmptyUrl (
    val message: String = "url must be valid or empty",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = []
)

class ValidOrEmptyUrlValidator : ConstraintValidator<ValidOrEmptyUrl, String> {
    override fun isValid(value: String?, context: ConstraintValidatorContext?): Boolean {
        return value.isNullOrBlank() || UrlValidator.getInstance().isValid(value)
    }
}