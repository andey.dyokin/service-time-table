package com.github.belbli.exceptions

import org.springframework.http.HttpStatus

class NotFoundException(message: String?) : ApplicationException(message, HttpStatus.NOT_FOUND)