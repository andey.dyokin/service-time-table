package com.github.belbli.exceptions

import org.springframework.http.HttpStatus


open class ApplicationException(override val message: String?,
                                private val status: HttpStatus) : RuntimeException(message)