package com.github.belbli.repository

import com.github.belbli.model.Meeting
import org.springframework.data.jpa.repository.EntityGraph
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface MeetingRepository : PagingAndSortingRepository<Meeting?, UUID?> {

}