package com.github.belbli.repository

import com.github.belbli.model.Notification
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface NotificationRepository : PagingAndSortingRepository<Notification?, UUID?> {
    fun findAllByRecipientId(recipientId: UUID): List<Notification>
}