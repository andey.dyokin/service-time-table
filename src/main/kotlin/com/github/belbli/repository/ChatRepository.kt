package com.github.belbli.repository

import com.github.belbli.model.Chat
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface ChatRepository : PagingAndSortingRepository<Chat?, UUID?> {
    fun findUserChatsByMembersPersonInfoId(personInfoId: UUID, pageable: Pageable): Page<Chat>
}