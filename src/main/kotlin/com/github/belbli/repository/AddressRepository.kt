package com.github.belbli.repository

import com.github.belbli.model.Address
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface AddressRepository : PagingAndSortingRepository<Address, Long>