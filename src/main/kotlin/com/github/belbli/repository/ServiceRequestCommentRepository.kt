package com.github.belbli.repository

import com.github.belbli.model.ServiceRequestComment
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface ServiceRequestCommentRepository: PagingAndSortingRepository<ServiceRequestComment, UUID> {
}