package com.github.belbli.repository

import com.github.belbli.model.ServiceRequest
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface ServiceRequestRepository: PagingAndSortingRepository<ServiceRequest, UUID> {
    fun findAllByExecutorSpecialistId(specialistId: UUID): List<ServiceRequest>

    fun findAllByCustomerClientId(clientId: UUID): List<ServiceRequest>

    fun findAllByCustomerClientId(clientId: UUID, pageable: Pageable): Page<ServiceRequest>

    fun findServiceRequestsByExecutorOrganizationOrganizationId(organizationId: UUID, pageable: Pageable): Page<ServiceRequest>
}