package com.github.belbli.repository

import com.github.belbli.model.PersonInfo
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface PersonInfoRepository : PagingAndSortingRepository<PersonInfo?, UUID?> {
    fun findByEmail(email: String): PersonInfo?
    fun findByEmailAndPassword(email: String, password: String): PersonInfo?
    fun findAllByPersonInfoIdIn(personInfoIds: List<UUID>): List<PersonInfo>
    fun findAllByEmailIn(emails: List<String>): List<PersonInfo>
    fun findByEmailContainsOrFirstnameContainsOrLastnameContains(email: String, firstname: String, lastname: String, pageable: Pageable): Page<PersonInfo>
}