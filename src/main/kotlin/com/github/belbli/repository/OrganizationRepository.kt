package com.github.belbli.repository

import com.github.belbli.model.Organization
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface OrganizationRepository : PagingAndSortingRepository<Organization?, UUID?> {
    fun findByName(organizationName: String?): Organization?

    fun findByNameContainingIgnoreCase(name: String, page: Pageable): Page<Organization>
}