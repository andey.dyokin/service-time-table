package com.github.belbli.repository

import com.github.belbli.model.RootUser
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface RootUserRepository: PagingAndSortingRepository<RootUser, UUID> {
    fun findByPersonInfoPersonInfoId(personInfoId: UUID): RootUser?

    fun findByPersonInfoEmail(email: String): RootUser?
}