package com.github.belbli.repository

import com.github.belbli.model.OrganizationDomain
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface OrganizationDomainRepository: PagingAndSortingRepository<OrganizationDomain?, Int?> {
    fun findByDomainName(domainName: String): OrganizationDomain?
}