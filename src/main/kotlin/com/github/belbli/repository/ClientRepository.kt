package com.github.belbli.repository

import com.github.belbli.model.Client
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ClientRepository : PagingAndSortingRepository<Client?, UUID?> {
    fun findAllById(id: Iterable<UUID>): List<Client>

    fun findByPersonInfoPersonInfoId(personInfoId: UUID): Client?

    fun findAllByPersonInfoEmailIn(emails: List<String>): List<Client>
}