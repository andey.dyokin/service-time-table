package com.github.belbli.repository

import com.github.belbli.model.Specialist
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface SpecialistRepository : PagingAndSortingRepository<Specialist?, UUID?> {
    fun findSpecialistsByOrganizationOrganizationId(organizationId: UUID, pageable: Pageable): Page<Specialist>

    fun findByPersonInfoPersonInfoId(personInfoId: UUID): Specialist?

    fun findByOrganizationOrganizationIdAndPersonInfoCreateDateBetween(organizationId: UUID, start: Date, end: Date): List<Specialist>
}