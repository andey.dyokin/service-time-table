package com.github.belbli.repository

import com.github.belbli.model.ChatMessage
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface ChatMessagesRepository : PagingAndSortingRepository<ChatMessage?, UUID?> {
}