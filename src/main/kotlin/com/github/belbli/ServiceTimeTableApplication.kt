package com.github.belbli

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@SpringBootApplication
class ServiceTimeTableApplication {
    @Bean
    fun executorService(): ExecutorService {
        return Executors.newFixedThreadPool(4)
    }
}

fun main(args: Array<String>) {
    SpringApplication.run(ServiceTimeTableApplication::class.java)
}