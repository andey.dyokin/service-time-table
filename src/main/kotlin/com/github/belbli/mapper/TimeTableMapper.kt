package com.github.belbli.mapper

import com.github.belbli.model.TimeTable
import com.github.belbli.model.dto.TimeTableDto
import org.mapstruct.Mapper

@Mapper(componentModel = "spring")
interface TimeTableMapper {
    fun toTimeTableDto (timeTable: TimeTable): TimeTableDto
}