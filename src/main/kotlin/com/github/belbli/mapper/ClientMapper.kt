package com.github.belbli.mapper

import com.github.belbli.model.Client
import com.github.belbli.model.dto.ClientDto
import com.github.belbli.model.requests.ClientDtoWithPassword
import org.mapstruct.Mapper
import org.mapstruct.NullValueCheckStrategy

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
interface ClientMapper {
    fun toClientDto (client: Client): ClientDto

    fun toClient(clientDto: ClientDtoWithPassword): Client
}