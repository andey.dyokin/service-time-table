package com.github.belbli.mapper

import com.github.belbli.model.Meeting
import com.github.belbli.model.dto.MeetingDto
import org.mapstruct.Mapper

@Mapper(componentModel = "spring")
interface MeetingMapper {
    fun toMeetingDto (Meeting: Meeting): MeetingDto

    fun toMeeting(MeetingDto: MeetingDto): Meeting

    fun toMeetingDtos (Meeting: List<Meeting>): List<MeetingDto>

    fun toMeetings(MeetingDto: List<MeetingDto>): List<Meeting>
}