package com.github.belbli.mapper

import com.github.belbli.model.ChatMessage
import com.github.belbli.model.dto.ChatMessageDto
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

@Mapper(componentModel = "spring")
interface ChatMessageMapper {
    @Mappings(
        Mapping(source = "chatMessage.chat.chatId", target = "chatId"),
    )
    fun toChatMessageDto (chatMessage: ChatMessage): ChatMessageDto

    fun toChatMessage(chatMessageDto: ChatMessageDto): ChatMessage
}