package com.github.belbli.mapper

import com.github.belbli.model.Organization
import com.github.belbli.model.ServiceRequest
import com.github.belbli.model.Specialist
import com.github.belbli.model.dto.AddressDto
import com.github.belbli.model.dto.ClientDto
import com.github.belbli.model.dto.MeetingDto
import com.github.belbli.model.dto.OrganizationDto
import com.github.belbli.model.dto.PersonInfoDto
import com.github.belbli.model.dto.SpecialistDto
import com.github.belbli.model.requests.OrganizationWithSpecialistPasswordDto
import com.github.belbli.model.response.OrganizationSearchResponse
import com.github.belbli.model.response.ServiceRequestResponse
import org.mapstruct.Context
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.Named

@Mapper(componentModel = "spring")
interface OrganizationMapper {

    @Mappings(
            Mapping(source = "organization.organizationDomain.domainName", target = "domainName"),
    )
    fun toOrganizationSearchResponse(organization: Organization): OrganizationSearchResponse

    //@Mapping(source = "specialists", target = "specialists", qualifiedByName = ["convertSpecialists"])
    fun toOrganizationDto(organization: Organization, @Context context: CycleAvoidingMappingContext): OrganizationDto

    /*companion object {
        @JvmStatic
        @Named("convertSpecialists")
        fun toSpecialistDtos(specialists: List<Specialist>): List<SpecialistDto> {
            return specialists.map {
                toSpecialistDto(it)
            }
        }

        fun toSpecialistDto(specialist: Specialist): SpecialistDto {
            return SpecialistDto(
                specialistId = specialist.specialistId,
                organizationId = specialist.organization?.organizationId,
                timeTableId = specialist.timeTable?.timeTableId,
                personInfo = PersonInfoDto(
                    personInfoId = specialist.personInfo.personInfoId,
                    firstname = specialist.personInfo.firstname,
                    lastname = specialist.personInfo.lastname,
                    email = specialist.personInfo.email,
                    image = specialist.personInfo.image
                ),
                workAddress = AddressDto(
                    addressId = specialist.workAddress.addressId,
                    country = specialist.workAddress.country,
                    city = specialist.workAddress.city,
                    street = specialist.workAddress.street,
                    building = specialist.workAddress.building,
                    office = specialist.workAddress.office
                ),
                role = specialist.role,
                experienceInYears = specialist.experienceInYears,
            )
        }
    }*/

    fun toOrganization(organizationDto: OrganizationWithSpecialistPasswordDto): Organization
}