package com.github.belbli.mapper

import com.github.belbli.model.RootUser
import com.github.belbli.model.dto.RootUserDto
import com.github.belbli.model.requests.RootUserAccountRequest
import org.mapstruct.Mapper

@Mapper(componentModel = "spring")
interface RootUserMapper {
    fun toRootUser(rootUserAccountRequest: RootUserAccountRequest): RootUser

    fun toRootUserDto(rootUser: RootUser): RootUserDto
}