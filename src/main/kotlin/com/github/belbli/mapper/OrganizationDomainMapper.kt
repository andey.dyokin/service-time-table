package com.github.belbli.mapper

import com.github.belbli.model.OrganizationDomain
import com.github.belbli.model.dto.OrganizationDomainDto
import org.mapstruct.Mapper

@Mapper(componentModel = "spring")
interface OrganizationDomainMapper {
    fun toOrganizationDomainDto (OrganizationDomain: OrganizationDomain): OrganizationDomainDto

    fun toOrganizationDomain(OrganizationDomainDto: OrganizationDomainDto): OrganizationDomain

    fun toOrganizationDomainDtos (OrganizationDomain: List<OrganizationDomain>): List<OrganizationDomainDto>

    fun toOrganizationDomains(OrganizationDomainDto: List<OrganizationDomainDto>): List<OrganizationDomain>
}