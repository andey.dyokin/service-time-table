package com.github.belbli.mapper

import com.github.belbli.model.ServiceRequestComment
import com.github.belbli.model.dto.ServiceRequestCommentDto
import org.mapstruct.Mapper

@Mapper(componentModel = "spring")
interface ServiceRequestCommentMapper {
    fun toServiceRequestDto (serviceRequest: ServiceRequestComment): ServiceRequestCommentDto
}