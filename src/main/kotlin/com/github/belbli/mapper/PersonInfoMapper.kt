package com.github.belbli.mapper

import com.github.belbli.model.PersonInfo
import com.github.belbli.model.dto.PersonInfoDto
import org.mapstruct.Mapper

@Mapper(componentModel = "spring")
interface PersonInfoMapper {
    fun toPersonInfoDto (personInfo: PersonInfo): PersonInfoDto

    fun toPersonInfo(personInfoDto: PersonInfoDto): PersonInfo

    fun toPersonInfoDtos (personInfo: List<PersonInfo>): List<PersonInfoDto>

    fun toPersonInfos(personInfoDto: List<PersonInfoDto>): List<PersonInfo>
}