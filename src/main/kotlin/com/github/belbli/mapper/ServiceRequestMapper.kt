package com.github.belbli.mapper

import com.github.belbli.model.ServiceRequest
import com.github.belbli.model.response.ServiceRequestResponse
import org.mapstruct.Context
import org.mapstruct.Mapper

@Mapper(componentModel = "spring")
interface ServiceRequestMapper {
    fun toServiceRequestDto (serviceRequest: ServiceRequest): ServiceRequestResponse

    fun toServiceRequest(serviceRequestResponse: ServiceRequestResponse): ServiceRequest

    fun toServiceRequestDtos (serviceRequests: List<ServiceRequest>, @Context context: CycleAvoidingMappingContext): List<ServiceRequestResponse>

    fun toServiceRequests(serviceRequestResponses: List<ServiceRequestResponse>): List<ServiceRequest>
}