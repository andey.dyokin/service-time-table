package com.github.belbli.mapper

import com.github.belbli.model.Specialist
import com.github.belbli.model.dto.SpecialistDto
import com.github.belbli.model.requests.SpecialistDtoWithPassword
import com.github.belbli.model.response.SpecialistResponse
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

@Mapper(componentModel = "spring")
interface SpecialistMapper {
    @Mappings(
        Mapping(source = "organization.organizationId", target = "organizationId"),
        Mapping(source = "timeTable.timeTableId", target = "timeTableId"),
    )
    fun toSpecialistDto (specialist: Specialist): SpecialistDto

    @Mappings(
            Mapping(source = "timeTable.timeTableId", target = "timeTableId"),
    )
    fun toSpecialistResponse(specialist: Specialist): SpecialistResponse

    fun toSpecialist(specialistDto: SpecialistDtoWithPassword): Specialist

    fun toSpecialistDtos (specialist: List<Specialist>): List<SpecialistDto>

    fun toSpecialists(specialistDto: List<SpecialistDtoWithPassword>): List<Specialist>
}