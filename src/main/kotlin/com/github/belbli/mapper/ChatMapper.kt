package com.github.belbli.mapper

import com.github.belbli.model.Chat
import com.github.belbli.model.ChatMessage
import com.github.belbli.model.dto.ChatDto
import com.github.belbli.model.dto.ChatMessageDto
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

@Mapper(componentModel = "spring")
interface ChatMapper {
    fun toChatDto (chat: Chat): ChatDto

    fun toChat(chatDto: ChatDto): Chat

    @Mappings(
        Mapping(source = "chatMessage.chat.chatId", target = "chatId"),
    )
    fun chatMessageToChatMessageDto (chatMessage: ChatMessage): ChatMessageDto
}