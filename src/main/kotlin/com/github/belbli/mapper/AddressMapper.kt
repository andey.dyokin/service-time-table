package com.github.belbli.mapper

import com.github.belbli.model.Address
import com.github.belbli.model.dto.AddressDto
import org.mapstruct.Mapper

@Mapper(componentModel = "spring")
interface AddressMapper {
    fun toAddressDto (address: Address): AddressDto

    fun toAddress(addressDto: AddressDto): Address

    fun toAddressDtos (address: List<Address>): List<AddressDto>

    fun toAddresses(addressDto: List<AddressDto>): List<Address>
}