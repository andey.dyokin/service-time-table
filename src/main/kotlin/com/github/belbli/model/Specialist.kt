package com.github.belbli.model

import org.hibernate.annotations.GenericGenerator
import java.util.*
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.OneToOne

@Entity
data class Specialist (
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "specialist_id", updatable = false, nullable = false)
    val specialistId: UUID?,

    @ManyToOne
    @JoinColumn(name = "fk_organization")
    val organization: Organization?,

    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "fk_person_info", unique = true)
    val personInfo: PersonInfo,

    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "fk_time_table")
    val timeTable: TimeTable?,

    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "fk_address")
    val workAddress: Address,
    val role: String,
    val experienceInYears: String,

    @OneToMany(mappedBy = "executor", cascade = [CascadeType.REMOVE])
    val serviceRequests: List<ServiceRequest>?
)