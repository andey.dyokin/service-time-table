package com.github.belbli.model

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.UpdateTimestamp
import org.springframework.data.jpa.repository.Temporal
import java.util.Date
import java.util.UUID
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.TemporalType

@Entity
data class ChatMessage(
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "message_id", updatable = false, nullable = false)
    val messageId: UUID?,

    @Column(length = 1024*32)
    val text: String,

    @ManyToOne(cascade = [CascadeType.MERGE])
    @JoinColumn(name = "fk_person_info", unique = true)
    val sender: PersonInfo,

    @ManyToOne
    @JoinColumn(name = "fk_chat", nullable = false)
    val chat: Chat,

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date")
    val createDate: Date = Date(),

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_date")
    val updateDate: Date = Date(),
)