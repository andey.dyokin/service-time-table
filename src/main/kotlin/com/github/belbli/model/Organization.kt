package com.github.belbli.model

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.Fetch
import org.hibernate.annotations.FetchMode
import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.UpdateTimestamp
import org.springframework.data.jpa.repository.Temporal
import java.util.*
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.TemporalType

@Entity
data class Organization (
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    val organizationId: UUID?,
    val name: String,
    val description: String,

    @OneToMany(cascade = [CascadeType.ALL])
    @Fetch(FetchMode.SUBSELECT)
    val addresses: List<Address>,

    @OneToMany(mappedBy = "organization", cascade = [CascadeType.REMOVE])
    @Fetch(FetchMode.SUBSELECT)
    val specialists: List<Specialist>,

    @OneToOne
    @JoinColumn(name = "fk_root_user")
    @Fetch(FetchMode.JOIN)
    val rootUser: RootUser?,

    @ManyToOne
    @JoinColumn(name = "fk_organization_domain")
    val organizationDomain: OrganizationDomain,

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date")
    val createDate: Date = Date(),

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_date")
    val updateDate: Date = Date(),

    val logo: String?
)