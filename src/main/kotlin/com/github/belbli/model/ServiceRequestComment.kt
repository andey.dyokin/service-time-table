package com.github.belbli.model

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.UpdateTimestamp
import org.springframework.data.jpa.repository.Temporal
import java.util.Date
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.TemporalType

@Entity
data class ServiceRequestComment(
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "comment_id", updatable = false, nullable = false)
    val commentId: UUID?,

    @Column(length = 20480)
    val message: String,

    @ManyToOne
    @JoinColumn(name = "fk_service_request", nullable = false)
    val serviceRequest: ServiceRequest,

    @ManyToOne
    @JoinColumn(name = "fk_person_info", nullable = false)
    val commenter: PersonInfo,

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date")
    val createDate: Date = Date(),

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_date")
    val updateDate: Date = Date(),
)
