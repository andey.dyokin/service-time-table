package com.github.belbli.model

import org.hibernate.annotations.GenericGenerator
import java.util.UUID
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.OrderBy

@Entity
data class Chat(
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "chat_id", updatable = false, nullable = false)
    val chatId: UUID?,

    val name: String,

    val chatImage: String?,

    @ManyToOne(cascade = [CascadeType.MERGE, CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH])
    @JoinColumn(name = "fk_person_info", unique = true)
    val chatCreator: PersonInfo,

    @OrderBy("createDate")
    @OneToMany(mappedBy = "chat")
    val messages: MutableList<ChatMessage>,

    @OneToMany(cascade = [CascadeType.MERGE, CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH])
    val members: MutableList<PersonInfo>
)
