package com.github.belbli.model.dto

data class CredentialsDto(
        val email: String,
        val password: String
)
