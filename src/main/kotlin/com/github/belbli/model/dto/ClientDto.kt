package com.github.belbli.model.dto

import com.github.belbli.model.response.ServiceRequestResponse
import java.util.*

data class ClientDto(
    val clientId: UUID?,
    val personInfo: PersonInfoDto,
)
