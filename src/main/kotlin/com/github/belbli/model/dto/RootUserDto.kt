package com.github.belbli.model.dto

import java.util.*

data class RootUserDto(
    val rootUserId: UUID?,
    val personInfo: PersonInfoDto,
    val organization: OrganizationDto?
)
