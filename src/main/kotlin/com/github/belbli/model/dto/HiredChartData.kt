package com.github.belbli.model.dto

import com.github.belbli.model.UserRole

data class HiredChartData(
        val specialistsByRoleCount: Map<String, Int>,
        val specialistsByCountry: Map<String, Int>,
        val specialistsByCity: Map<String, Int>,
        val specialistsByExperience: Map<String, Int>,
)
