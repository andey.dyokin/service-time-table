package com.github.belbli.model.dto

import com.github.belbli.model.response.ServiceRequestResponse
import java.util.*
import javax.validation.Valid
import javax.validation.constraints.NotBlank


data class SpecialistDto (
        val specialistId: UUID?,
        val organizationId: UUID?,
        val timeTableId: UUID?,
        @field:Valid val personInfo: PersonInfoDto,
        @field:Valid val workAddress: AddressDto,
        @field:NotBlank val role: String,
        @field:NotBlank val experienceInYears: String,
)