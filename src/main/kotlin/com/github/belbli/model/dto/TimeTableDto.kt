package com.github.belbli.model.dto

import java.util.UUID

data class TimeTableDto(
        val timeTableId: UUID?,
        val meetings: MutableList<MeetingDto>?
)