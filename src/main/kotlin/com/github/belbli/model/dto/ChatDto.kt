package com.github.belbli.model.dto

import java.util.UUID

data class ChatDto(
    val chatId: UUID,
    val name: String,
    val chatImage: String?,
    val chatCreator: PersonInfoDto,
    val messages: List<ChatMessageDto>,
    val members: List<PersonInfoDto>
)
