package com.github.belbli.model.dto

import java.util.*

data class ServiceRequestCommentDto(
    val commentId: UUID?,
    val message: String,
    val commenter: PersonInfoDto,
    val createDate: Date = Date(),
    val updateDate: Date = Date(),
)
