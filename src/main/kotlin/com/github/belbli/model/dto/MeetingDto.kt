package com.github.belbli.model.dto

import com.github.belbli.model.MeetingStatus
import java.util.*
import javax.validation.Valid


data class MeetingDto (
    val meetingId: UUID?,
    val description: String ?= "Description",
    val subject: String ?= "Subject",
    val startDate: Date,
    val endDate: Date,
    val address: AddressDto,
    @field:Valid val organizer: SpecialistDto,
    @field:Valid val participants: List<ClientDto>,
    val status: MeetingStatus = MeetingStatus.PLANNED
)
