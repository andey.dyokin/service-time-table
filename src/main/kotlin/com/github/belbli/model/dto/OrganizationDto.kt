package com.github.belbli.model.dto

import com.github.belbli.validators.ValidOrEmptyUrl
import java.util.*
import javax.validation.Valid
import javax.validation.constraints.NotBlank

data class OrganizationDto (val organizationId: UUID,
                            @field:NotBlank val name: String,
                            @field:NotBlank val description: String,
                            @field:Valid val addresses: List<AddressDto>?,
                            @field:Valid val specialists: List<SpecialistDto>?,
                            val organizationDomain: OrganizationDomainDto,
                            val createDate: Date = Date(),
                            @field:ValidOrEmptyUrl val logo: String?
)