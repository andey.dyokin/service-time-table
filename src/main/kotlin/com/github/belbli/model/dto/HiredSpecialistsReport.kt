package com.github.belbli.model.dto

data class HiredSpecialistsReport(
        val hiredSpecialistsMap: Map<String, SpecialistDto>
)
