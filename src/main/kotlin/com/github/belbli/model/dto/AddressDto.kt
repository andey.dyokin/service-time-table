package com.github.belbli.model.dto

import javax.validation.constraints.NotBlank

data class AddressDto (
        val addressId: Long?,
        @field:NotBlank val country: String,
        @field:NotBlank val city: String,
        @field:NotBlank val street: String,
        @field:NotBlank val building: String,
        @field:NotBlank val office: String,
)