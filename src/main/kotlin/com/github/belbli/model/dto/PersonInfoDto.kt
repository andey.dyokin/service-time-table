package com.github.belbli.model.dto

import com.github.belbli.model.UserRole
import com.github.belbli.validators.ValidOrEmptyUrl
import java.util.*
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size


data class PersonInfoDto (
        val personInfoId: UUID?,
        @field:Size(min = 1, max = 255, message = "firstname must be less than 255") val firstname: String,
        @field:Size(min = 1, max = 255, message = "lastname must be less than 255") val lastname: String,

        @field:Pattern(regexp = ".+\\@.+\\..+", message = "email not valid")
        val email: String,

        @field:ValidOrEmptyUrl
        val image: String?,
        val userRole: UserRole = UserRole.UNKNOWN,
        val createDate: Date = Date(),
        val updateDate: Date = Date(),
)