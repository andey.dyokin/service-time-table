package com.github.belbli.model.dto

import java.util.Date
import java.util.UUID

data class ChatMessageDto(
    val messageId: UUID,
    val text: String,
    val sender: PersonInfoDto,
    val chatId: UUID,
    val createDate: Date = Date(),
    val updateDate: Date = Date(),
)
