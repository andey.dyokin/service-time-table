package com.github.belbli.model.dto

data class OrganizationDomainDto(
        val domainId: Int?,
        val domainName: String ?= "other",
)
