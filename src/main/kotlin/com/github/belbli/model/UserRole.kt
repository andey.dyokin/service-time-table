package com.github.belbli.model

enum class UserRole {
    ROOT_USER,
    SPECIALIST,
    CLIENT,
    UNKNOWN
}