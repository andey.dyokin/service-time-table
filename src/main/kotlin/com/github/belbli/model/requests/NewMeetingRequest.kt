package com.github.belbli.model.requests

import com.github.belbli.model.dto.AddressDto
import java.util.*

data class NewMeetingRequest(
    val organizerId: UUID,
    val participantEmails: List<String>,
    val address: AddressDto,
    val startDate: Date,
    val endDate: Date,
    val description: String ?= "Description",
    val subject: String ?= "Subject",
)
