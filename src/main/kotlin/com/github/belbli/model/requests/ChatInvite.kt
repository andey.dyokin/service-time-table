package com.github.belbli.model.requests

import java.util.UUID

data class ChatInvite(
    val userEmails: Set<String>,
    val inviteSenderPersonInfoId: UUID,
    val message: String
)