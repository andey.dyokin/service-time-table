package com.github.belbli.model.requests

import java.util.UUID

data class NewMessageRequest(
    val senderId: UUID,
    val chatId: UUID,
    val message: String
)