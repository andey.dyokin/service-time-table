package com.github.belbli.model.requests

import org.springframework.data.domain.Sort
import javax.validation.constraints.Size

data class PageRequest(
        val page: Int,
        @Size(min = 5, max = 50)
        val size: Int,
        val sort: Sort?
)
