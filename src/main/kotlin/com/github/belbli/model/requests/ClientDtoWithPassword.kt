package com.github.belbli.model.requests

import com.github.belbli.model.Meeting
import javax.validation.Valid

data class ClientDtoWithPassword(
        @field:Valid val personInfo: PersonInfoDtoWithPassword,
        val meetings: MutableList<Meeting> = mutableListOf()
)
