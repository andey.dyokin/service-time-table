package com.github.belbli.model.requests

data class ChatCreationRequest(
    val chatName: String,
    val chatImage: String?,
    val memberEmails: List<String>?
)
