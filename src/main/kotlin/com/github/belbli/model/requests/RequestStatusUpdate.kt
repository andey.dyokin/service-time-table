package com.github.belbli.model.requests

import com.github.belbli.model.RequestStatus

data class RequestStatusUpdate(
    val newStatus: RequestStatus
)