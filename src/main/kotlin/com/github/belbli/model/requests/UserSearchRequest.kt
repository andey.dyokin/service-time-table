package com.github.belbli.model.requests

data class UserSearchRequest(
    val searchedPart: String
)
