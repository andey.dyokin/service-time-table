package com.github.belbli.model.requests

data class MessageUpdateRequest(
    val newMessage: String
)
