package com.github.belbli.model.requests

import com.github.belbli.model.dto.AddressDto
import com.github.belbli.model.dto.PersonInfoDto
import java.util.*
import javax.validation.Valid
import javax.validation.constraints.NotBlank

data class SpecialistDtoWithPassword(
        val specialistId: UUID?,
        val organizationId: UUID?,
        val timeTableId: UUID?,
        @field:Valid val personInfo: PersonInfoDtoWithPassword,
        @field:Valid val workAddress: AddressDto,
        @field:NotBlank val role: String,
        @field:NotBlank val experienceInYears: String,
)
