package com.github.belbli.model.requests

data class ChatRenameRequest(
    val newName: String
)
