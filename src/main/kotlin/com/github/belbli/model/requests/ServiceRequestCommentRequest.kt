package com.github.belbli.model.requests

data class ServiceRequestCommentRequest(
    val comment: String
)
