package com.github.belbli.model.requests

import com.github.belbli.model.UserRole
import com.github.belbli.validators.ValidOrEmptyUrl
import java.util.Date
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

data class PersonInfoDtoWithPassword(
    @field:Size(min = 1, max = 255, message = "firstname must be less than 255") val firstname: String,
    @field:Size(min = 1, max = 255, message = "lastname must be less than 255") val lastname: String,

    @field:Pattern(regexp = ".+\\@.+\\..+", message = "email not valid")
    val email: String,
    @field:Size(min = 8, max = 255, message = "password length should be between 8 and 255 characters")
    val password: String,

    @field:ValidOrEmptyUrl
    val image: String?,
    val userRole: UserRole = UserRole.UNKNOWN,

    val createDate: Date = Date(),
    val updateDate: Date = Date(),
)
