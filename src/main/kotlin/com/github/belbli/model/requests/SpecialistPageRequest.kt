package com.github.belbli.model.requests

import org.springframework.data.domain.Sort
import java.util.UUID

data class SpecialistPageRequest(
        val organizationId: UUID,
        val pageRequest: PageRequest
) {

    fun toSpringPageRequest(): org.springframework.data.domain.PageRequest {
        return org.springframework.data.domain.PageRequest.of(
                pageRequest.page,
                pageRequest.size,
                pageRequest.sort ?: Sort.unsorted()
        )
    }
}
