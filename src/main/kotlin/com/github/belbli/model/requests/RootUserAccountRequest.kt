package com.github.belbli.model.requests

import javax.validation.Valid

data class RootUserAccountRequest(
    @field:Valid val personInfo: PersonInfoDtoWithPassword
)