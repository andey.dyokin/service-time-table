package com.github.belbli.model.requests

data class LeaveChatRequest(
    val userEmail: String
)
