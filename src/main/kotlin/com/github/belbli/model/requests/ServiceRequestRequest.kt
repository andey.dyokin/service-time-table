package com.github.belbli.model.requests

import java.util.*

data class ServiceRequestRequest(
    val subject: String,
    val comment: String,
    val customerId: UUID,
    val executorId: UUID,
)