package com.github.belbli.model.requests

data class PasswordUpdateRequest(
    val oldPassword: String,
    val newPassword: String
)
