package com.github.belbli.model.requests

import com.github.belbli.model.dto.AddressDto
import com.github.belbli.model.dto.OrganizationDomainDto
import com.github.belbli.model.dto.SpecialistDto
import com.github.belbli.validators.ValidOrEmptyUrl
import java.util.*
import javax.validation.Valid
import javax.validation.constraints.NotBlank

data class OrganizationWithSpecialistPasswordDto(
        val rootUserId: UUID?,
        @field:NotBlank val name: String,
        @field:NotBlank val description: String,
        @field:Valid val addresses: List<AddressDto>?,
        @field:Valid val specialists: List<SpecialistDtoWithPassword>?,
        val organizationDomain: OrganizationDomainDto,
        val createDate: Date = Date(),
        val updateDate: Date = Date(),
        @field:ValidOrEmptyUrl val logo: String?
)
