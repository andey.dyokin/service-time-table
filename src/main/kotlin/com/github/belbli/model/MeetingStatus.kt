package com.github.belbli.model

enum class MeetingStatus(status: String) {
    PLANNED("planned"),
    CANCELLED("cancelled"),
    STARTED("started"),
    ENDED("ended");

}