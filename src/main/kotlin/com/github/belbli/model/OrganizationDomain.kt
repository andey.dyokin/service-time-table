package com.github.belbli.model

import org.hibernate.annotations.Fetch
import org.hibernate.annotations.FetchMode
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
data class OrganizationDomain(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "domain_id")
    val domainId: Int?,
    val domainName: String,

    @OneToMany(mappedBy = "organizationDomain")
    val organizations: List<Organization> ?= emptyList()
)
