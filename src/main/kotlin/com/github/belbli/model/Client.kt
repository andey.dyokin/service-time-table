package com.github.belbli.model

import org.hibernate.annotations.GenericGenerator
import java.util.*
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.OneToMany
import javax.persistence.OneToOne

@Entity
data class Client (
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "client_id", updatable = false, nullable = false)
    val clientId: UUID?,

    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "person_info_id", unique = true)
    val personInfo: PersonInfo,

    @ManyToMany(cascade = [ CascadeType.ALL ], fetch = FetchType.EAGER)
    @JoinTable(
        name = "client_meeting",
        joinColumns = [ JoinColumn(name = "client_id") ],
        inverseJoinColumns = [ JoinColumn(name = "meeting_id") ]
    )
    val meetings: MutableList<Meeting> = mutableListOf(),

    @OneToMany(mappedBy = "customer", cascade = [CascadeType.REMOVE])
    val serviceRequests: List<ServiceRequest>?
)