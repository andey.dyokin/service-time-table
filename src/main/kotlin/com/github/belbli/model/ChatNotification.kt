package com.github.belbli.model

import java.util.*

data class ChatNotification(
    val chatId: UUID,
    val senderId: UUID,
    val text: String
)
