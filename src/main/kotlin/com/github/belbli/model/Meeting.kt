package com.github.belbli.model

import org.hibernate.annotations.Fetch
import org.hibernate.annotations.FetchMode
import org.hibernate.annotations.GenericGenerator
import java.util.UUID
import java.util.Date
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne
import javax.persistence.OneToOne

@Entity
data class Meeting (
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "meeting_id", updatable = false, nullable = false)
    val meetingId: UUID?,
    val timeTableId: UUID?,
    val startDate: Date,
    val endDate: Date,
    val description: String ?= "Description",
    val subject: String ?= "Subject",

    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "fk_address", unique = true)
    val address: Address,

    @OneToOne
    @JoinColumn(name = "fk_organizer")
    val organizer: Specialist,

    @ManyToMany(mappedBy = "meetings")
    @Fetch(FetchMode.JOIN)
    val participants: List<Client>?,

    @Enumerated(value = EnumType.STRING)
    val status: MeetingStatus = MeetingStatus.PLANNED
)