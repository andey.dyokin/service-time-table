package com.github.belbli.model

import org.hibernate.annotations.GenericGenerator
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToMany
import javax.persistence.OrderBy

@Entity
data class TimeTable (
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "time_table_id", updatable = false, nullable = false)
    val timeTableId: UUID?,

    @OneToMany(fetch = FetchType.EAGER)
    @OrderBy("startDate ASC")
    @JoinColumn(name = "fk_timetable", unique = true)
    val meetings: MutableList<Meeting> = mutableListOf()
)