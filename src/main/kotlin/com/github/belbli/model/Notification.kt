package com.github.belbli.model

import org.hibernate.annotations.GenericGenerator
import org.springframework.data.jpa.repository.Temporal
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.TemporalType

@Entity
data class Notification(
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "chat_id", updatable = false, nullable = false)
    val notificationId: UUID?,

    val text: String,

    @Enumerated(value = EnumType.STRING)
    val eventType: NotificationEventType,

    val redirectId: UUID,

    val recipientId: UUID,

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date")
    val createDate: Date = Date(),
)
