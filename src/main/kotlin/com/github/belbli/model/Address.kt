package com.github.belbli.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class Address (
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "address_id")
    val addressId: Long?,
    val country: String,
    val city: String,
    val street: String,
    val building: String,
    val office: String,
)