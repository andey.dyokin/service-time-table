package com.github.belbli.model

import org.hibernate.annotations.GenericGenerator
import java.util.UUID
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne

@Entity
data class RootUser(
        @Id
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        @Column(name = "root_user_id", updatable = false, nullable = false)
        val rootUserId: UUID?,

        @OneToOne(cascade = [CascadeType.ALL])
        @JoinColumn(name = "fk_person_info", unique = true)
        val personInfo: PersonInfo,

        @OneToOne(mappedBy = "rootUser",cascade = [CascadeType.ALL])
        val organization: Organization?
)
