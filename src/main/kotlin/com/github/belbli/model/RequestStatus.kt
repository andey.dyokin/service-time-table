package com.github.belbli.model

enum class RequestStatus {
    PENDING,
    APPROVED,
    DECLINED,
    DONE
}