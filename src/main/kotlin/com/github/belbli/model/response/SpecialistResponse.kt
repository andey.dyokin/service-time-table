package com.github.belbli.model.response

import com.github.belbli.model.dto.AddressDto
import com.github.belbli.model.dto.OrganizationDto
import com.github.belbli.model.dto.PersonInfoDto
import java.util.*
import javax.validation.Valid
import javax.validation.constraints.NotBlank

data class SpecialistResponse (
    val specialistId: UUID?,
    val organization: OrganizationDto,
    val timeTableId: UUID?,

    @field:Valid
    val personInfo: PersonInfoDto,

    @field:Valid
    val workAddress: AddressDto,

    @field:NotBlank
    val role: String,

    @field:NotBlank
    val experienceInYears: String
)