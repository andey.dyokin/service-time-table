package com.github.belbli.model.response

import com.github.belbli.model.RequestStatus
import com.github.belbli.model.ServiceRequestComment
import com.github.belbli.model.dto.ClientDto
import com.github.belbli.model.dto.ServiceRequestCommentDto
import com.github.belbli.model.dto.SpecialistDto
import java.util.*

data class ServiceRequestResponse(
    val requestId: UUID?,
    val subject: String,
    val comment: String,
    val customer: ClientDto,
    val executor: SpecialistDto,
    val status: RequestStatus = RequestStatus.PENDING,
    val serviceRequestComments: List<ServiceRequestCommentDto>?
)