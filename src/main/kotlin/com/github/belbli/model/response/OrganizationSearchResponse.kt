package com.github.belbli.model.response

import com.github.belbli.model.dto.AddressDto
import com.github.belbli.model.dto.OrganizationDomainDto
import com.github.belbli.model.dto.SpecialistDto
import com.github.belbli.validators.ValidOrEmptyUrl
import java.util.*
import javax.validation.Valid
import javax.validation.constraints.NotBlank

data class OrganizationSearchResponse (
        val organizationId: UUID,
        @field:NotBlank val name: String,
        @field:NotBlank val description: String,
        @field:Valid val addresses: List<AddressDto>?,
        val domainName: String?,
        val organizationDomain: OrganizationDomainDto,
        val createDate: Date = Date(),
        @field:ValidOrEmptyUrl val logo: String?
)