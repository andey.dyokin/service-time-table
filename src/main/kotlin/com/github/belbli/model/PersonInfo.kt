package com.github.belbli.model

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.UpdateTimestamp
import org.springframework.data.jpa.repository.Temporal
import java.util.Date
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.TemporalType
import javax.validation.constraints.Email
import javax.validation.constraints.Size

@Entity
data class PersonInfo(
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "person_info_id", updatable = false, nullable = false)
    val personInfoId: UUID?,
    val firstname: String,
    val lastname: String,

    @field:Size(min = 8, max = 255, message = "password length should be between 8 and 255 characters")
    val password: String,

    @field:Email val email: String,
    val image: String?,

    @Enumerated(value = EnumType.STRING)
    val userRole: UserRole = UserRole.UNKNOWN,

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date")
    val createDate: Date = Date(),

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_date")
    val updateDate: Date = Date(),
)