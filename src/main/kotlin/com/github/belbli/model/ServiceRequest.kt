package com.github.belbli.model

import org.hibernate.annotations.Fetch
import org.hibernate.annotations.FetchMode
import org.hibernate.annotations.GenericGenerator
import java.util.*
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany

@Entity
data class ServiceRequest(
        @Id
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        @Column(name = "request_id", updatable = false, nullable = false)
        val requestId: UUID?,

        val subject: String,
        val comment: String,

        @ManyToOne
        @JoinColumn(name = "fk_client")
        val customer: Client,

        @ManyToOne
        @JoinColumn(name = "fk_specialist")
        val executor: Specialist,
        
        @Enumerated(value = EnumType.STRING)
        val status: RequestStatus = RequestStatus.PENDING,

        @OneToMany(mappedBy = "serviceRequest")
        val serviceRequestComments: MutableList<ServiceRequestComment>?
)
