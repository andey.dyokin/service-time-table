package com.github.belbli.model

enum class NotificationEventType {
    NEW_MESSAGE,
    SERVICE_REQUEST_STATUS_CHANGED,
    SERVICE_REQUEST_NEW_COMMENT,
    CHAT_INVITATION
}