package com.github.belbli.controller

import com.github.belbli.mapper.CycleAvoidingMappingContext
import com.github.belbli.mapper.OrganizationMapper
import com.github.belbli.mapper.RootUserMapper
import com.github.belbli.model.dto.HiredChartData
import com.github.belbli.model.dto.OrganizationDto
import com.github.belbli.model.dto.RootUserDto
import com.github.belbli.model.requests.OrganizationWithSpecialistPasswordDto
import com.github.belbli.service.OrganizationService
import com.github.belbli.service.ReportGeneratorService
import com.github.belbli.service.RootUserService
import org.apache.poi.hssf.usermodel.HeaderFooter.file
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.InputStreamResource
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.io.FileInputStream
import java.util.*
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid


@RestController
@RequestMapping("/api/v1/root-users")
class RootUserController @Autowired constructor(
        private val rootUserService: RootUserService,
        private val rootUserMapper: RootUserMapper,
        private val organizationService: OrganizationService,
        private val reportGeneratorService: ReportGeneratorService
) {
    @Autowired
    private lateinit var organizationMapper: OrganizationMapper

    @PostMapping("/organizations/new")
    fun createOrganization(@Valid @RequestBody organizationDto: OrganizationWithSpecialistPasswordDto): OrganizationDto {
        return organizationMapper.toOrganizationDto(
                organizationService.createOrganization(
                        organizationMapper.toOrganization(organizationDto),
                ), CycleAvoidingMappingContext()
        )
    }

    @GetMapping("/{rootUserId}")
    fun findRootUser(@PathVariable rootUserId: UUID): RootUserDto {
        return rootUserMapper
                .toRootUserDto(rootUserService.findRootUserById(rootUserId))
    }

    @GetMapping("/hired-specialists-report")
    fun generateHiredSpecialistsReport(
            @RequestParam("start") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX") start: Date,
            @RequestParam("end") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX") end: Date
    ): HiredChartData {
        return reportGeneratorService.generateHiredSpecialistsReport(start, end)
    }

    @GetMapping("/hired-specialists-report-xlxs")
    fun getHiredSpecialistsReport(
            @RequestParam("start") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX") start: Date,
            @RequestParam("end") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX") end: Date,
            response: HttpServletResponse
    ): ResponseEntity<ByteArray> {
        val report = reportGeneratorService.generateHiredSpecialistsReportXlxs(start, end)

        return createResponseEntity(report, "report.xlsx")
    }

    private fun createResponseEntity(
            report: ByteArray,
            fileName: String
    ): ResponseEntity<ByteArray> =
            ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"$fileName\"")
                    .body(report)
}