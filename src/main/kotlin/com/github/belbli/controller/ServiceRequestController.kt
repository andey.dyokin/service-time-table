package com.github.belbli.controller

import com.github.belbli.mapper.CycleAvoidingMappingContext
import com.github.belbli.mapper.ServiceRequestCommentMapper
import com.github.belbli.mapper.ServiceRequestMapper
import com.github.belbli.model.RequestStatus
import com.github.belbli.model.dto.ServiceRequestCommentDto
import com.github.belbli.model.requests.RequestStatusUpdate
import com.github.belbli.model.requests.ServiceRequestCommentRequest
import com.github.belbli.model.requests.ServiceRequestRequest
import com.github.belbli.model.response.ServiceRequestResponse
import com.github.belbli.service.ServiceRequestCommentService
import com.github.belbli.service.ServiceRequestService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.security.Principal
import java.util.*

@RestController
@RequestMapping("/api/v1/service-requests")
class ServiceRequestController @Autowired constructor(
    private val serviceRequestService: ServiceRequestService,
    private val serviceRequestCommentService: ServiceRequestCommentService
) {
    @Autowired
    private lateinit var serviceRequestMapper: ServiceRequestMapper

    @Autowired
    private lateinit var serviceRequestCommentMapper: ServiceRequestCommentMapper

    @PostMapping("/new")
    fun createNewServiceRequest(@RequestBody serviceRequest: ServiceRequestRequest): ServiceRequestResponse {
        return serviceRequestMapper.toServiceRequestDto(
            serviceRequestService.createNewServiceRequest(
                serviceRequest
            )
        )
    }

    @GetMapping("/statuses")
    fun getAllRequestStatuses(): Array<RequestStatus> {
        return RequestStatus.values()
    }

    @GetMapping("/specialist")
    fun findSpecialistRequests(@RequestParam(name="specialistId") specialistId: UUID, pageable: Pageable): List<ServiceRequestResponse> {
        return serviceRequestMapper.toServiceRequestDtos(
            serviceRequestService.findAllSpecialistRequests(
                specialistId
            ), CycleAvoidingMappingContext()
        )
    }

    @GetMapping("/client")
    fun findClientRequests(@RequestParam(name="clientId") clientId: UUID, pageable: Pageable): List<ServiceRequestResponse> {
        return serviceRequestMapper.toServiceRequestDtos(
            serviceRequestService.findAllClientRequests(
                clientId
            ), CycleAvoidingMappingContext()
        )
    }

    @PostMapping("/{serviceRequestId}/comments")
    fun saveComment(
        @PathVariable(name = "serviceRequestId") serviceRequestId: UUID,
        @RequestBody comment: ServiceRequestCommentRequest,
        principal: Principal
    ): ServiceRequestCommentDto {
        return serviceRequestCommentMapper.toServiceRequestDto(
            serviceRequestCommentService.saveRequestComment(serviceRequestId, principal, comment)
        )
    }

    @GetMapping("/{serviceRequestId}")
    fun findServiceRequestById(
        @PathVariable(name = "serviceRequestId") serviceRequestId: UUID
    ): ServiceRequestResponse {
        return serviceRequestMapper.toServiceRequestDto(
            serviceRequestService.findServiceRequestById(serviceRequestId)
        )
    }

    @PatchMapping("/{serviceRequestId}/status")
    fun updateServiceRequestStatus(
        @PathVariable(name = "serviceRequestId") serviceRequestId: UUID,
        @RequestBody statusUpdate: RequestStatusUpdate,
        principal: Principal
    ): ServiceRequestResponse {
        return serviceRequestMapper.toServiceRequestDto(
            serviceRequestService.updateServiceRequestStatus(serviceRequestId, statusUpdate, principal)
        )
    }
}