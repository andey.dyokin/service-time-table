package com.github.belbli.controller

import com.github.belbli.mapper.MeetingMapper
import com.github.belbli.mapper.TimeTableMapper
import com.github.belbli.model.TimeTable
import com.github.belbli.model.dto.MeetingDto
import com.github.belbli.model.dto.TimeTableDto
import com.github.belbli.model.requests.NewMeetingRequest
import com.github.belbli.service.TimetableService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/api/v1/timetables")
class TimetableController @Autowired constructor(
        private val timetableService: TimetableService,
) {
    @Autowired
    private lateinit var meetingMapper: MeetingMapper

    @Autowired
    private lateinit var timeTableMapper: TimeTableMapper

    @PostMapping("/meeting/new")
    fun addMeetingToTimeTable(@RequestBody newMeetingRequest: NewMeetingRequest): MeetingDto {
        return meetingMapper.toMeetingDto(
                timetableService.addMeeting(newMeetingRequest)
        )
    }

    @GetMapping("/{id}")
    fun getTimeTableById(@PathVariable("id") timeTableId: UUID): TimeTableDto {
        return timeTableMapper.toTimeTableDto(
                timetableService.getTimetableById(timeTableId)
        )
    }

    @GetMapping
    fun getTimetableByUserId(@RequestParam("specialistId") userId: UUID): TimeTableDto {
        return timeTableMapper.toTimeTableDto(
                timetableService.getTimetableByUserId(userId)
        )
    }
}