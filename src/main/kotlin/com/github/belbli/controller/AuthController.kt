package com.github.belbli.controller

import com.github.belbli.config.security.JwtProvider
import com.github.belbli.mapper.ClientMapper
import com.github.belbli.mapper.PersonInfoMapper
import com.github.belbli.mapper.RootUserMapper
import com.github.belbli.mapper.SpecialistMapper
import com.github.belbli.model.dto.ClientDto
import com.github.belbli.model.dto.CredentialsDto
import com.github.belbli.model.dto.PersonInfoDto
import com.github.belbli.model.dto.RootUserDto
import com.github.belbli.model.requests.ClientDtoWithPassword
import com.github.belbli.model.requests.PasswordUpdateRequest
import com.github.belbli.model.requests.RootUserAccountRequest
import com.github.belbli.model.requests.SpecialistDtoWithPassword
import com.github.belbli.service.ClientService
import com.github.belbli.service.PersonInfoService
import com.github.belbli.service.RootUserService
import com.github.belbli.service.SpecialistService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

@RestController
@RequestMapping("/api/v1/auth")
class AuthController @Autowired constructor(
        private val jwtProvider: JwtProvider,
        private val clientMapper: ClientMapper,
        private val rootUserMapper: RootUserMapper,
        private val specialistMapper: SpecialistMapper,
        private val personInfoMapper: PersonInfoMapper,
        private val clientService: ClientService,
        private val rootUserService: RootUserService,
        private val personInfoService: PersonInfoService,
        private val specialistService: SpecialistService,
) {
    companion object {
        private const val BEARER = "BEARER "
    }

    @PostMapping("/signin")
    fun signIn(
            @RequestBody credentialsDto: CredentialsDto,
            httpServletResponse: HttpServletResponse
    ): ResponseEntity<Any> {

        val user = personInfoService.findUserCredentials(credentialsDto)

        val token = jwtProvider.generateToken(credentialsDto.email)
        httpServletResponse.addHeader("Authorization", "$BEARER$token")
        return ResponseEntity.ok().body(user)

    }

    @PostMapping("/new/organization-account")
    fun createOrganizationAccount(
            @Valid @RequestBody rootUserAccountRequest: RootUserAccountRequest,
            httpServletResponse: HttpServletResponse
    ): RootUserDto {
        val rootUser = rootUserService.createRootUser(rootUserMapper.toRootUser(rootUserAccountRequest))
        val token = jwtProvider.generateToken(rootUserAccountRequest.personInfo.email)
        httpServletResponse.addHeader("Authorization", "$BEARER$token")
        return rootUserMapper.toRootUserDto(rootUser)
    }

    @PostMapping("/signup/specialist")
    fun createSpecialist(
            @RequestBody specialistDto: SpecialistDtoWithPassword,
            httpServletResponse: HttpServletResponse
    ): ResponseEntity<String> {
        specialistService.createSpecialist(specialistMapper.toSpecialist(specialistDto))
        val token = jwtProvider.generateToken(specialistDto.personInfo.email)
        httpServletResponse.addHeader("Authorization", "$BEARER$token")
        return ResponseEntity.ok().body("ok")
    }

    @PostMapping("/signup/client")
    fun createNewClient(
            @Valid @RequestBody clientDto: ClientDtoWithPassword,
            httpServletResponse: HttpServletResponse
    ): ResponseEntity<ClientDto> {
        val newClient = clientService.createClient(clientMapper.toClient(clientDto))
        val token = jwtProvider.generateToken(clientDto.personInfo.email)
        httpServletResponse.addHeader("Authorization", "$BEARER$token")
        return ResponseEntity.ok().body(
                clientMapper.toClientDto(newClient)
        )
    }

    @PostMapping("/password-update")
    fun updatePassword(@RequestBody passwordUpdateRequest: PasswordUpdateRequest): PersonInfoDto {
        val email = SecurityContextHolder.getContext().authentication.name

        val personInfo = personInfoService
                .findPersonInfoByCredentials(CredentialsDto(email, passwordUpdateRequest.oldPassword))

        return personInfoMapper.toPersonInfoDto(personInfoService.updatePersonInfo(
                personInfo.copy(
                        password = passwordUpdateRequest.newPassword
                )
        )
        )
    }
}