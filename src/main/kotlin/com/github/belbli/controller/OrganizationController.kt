package com.github.belbli.controller

import com.github.belbli.mapper.CycleAvoidingMappingContext
import com.github.belbli.mapper.OrganizationMapper
import com.github.belbli.mapper.ServiceRequestMapper
import com.github.belbli.model.ServiceRequest
import com.github.belbli.model.dto.OrganizationDto
import com.github.belbli.model.response.OrganizationSearchResponse
import com.github.belbli.model.response.ServiceRequestResponse
import com.github.belbli.service.OrganizationService
import com.github.belbli.service.ServiceRequestService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.User
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.security.Principal
import java.util.*


@RestController
@RequestMapping("/api/v1/organizations")
class OrganizationController(
    private val organizationService: OrganizationService,
    private val serviceRequestService: ServiceRequestService
) {
    @Autowired
    private lateinit var mapper: OrganizationMapper

    @Autowired
    private lateinit var serviceRequestMapper: ServiceRequestMapper


    @GetMapping
    fun findOrganizations(
        @RequestParam("namePart") namePart: String = "",
        pageable: Pageable = PageRequest.of(0, 15, Sort.by(Sort.Order.desc("createDate")))
    ): PageImpl<OrganizationSearchResponse> {
        val page = organizationService.findOrganizationsByName(namePart, pageable)
        val content = page.content.map { mapper.toOrganizationSearchResponse(it) }

        return PageImpl(content, page.pageable, page.totalElements)
    }

    @GetMapping("/{id}")
    fun findOrganizationById(@PathVariable id: UUID): OrganizationDto {
        return mapper.toOrganizationDto(
            organizationService.findOrganizationById(id),
            CycleAvoidingMappingContext()
        )
    }

    @GetMapping("/service-requests")
    fun findAllOrganizationRequests(principal: Principal, pageable: Pageable): Page<ServiceRequestResponse> {
        val name = principal.name
        val organizationRequests = serviceRequestService.findAllOrganizationRequest(name, pageable)
        val serviceRequestDtos = serviceRequestMapper.toServiceRequestDtos(
            organizationRequests.content, CycleAvoidingMappingContext()
        )

        return PageImpl(
            serviceRequestDtos,
            organizationRequests.pageable,
            organizationRequests.totalElements
        )
    }
}