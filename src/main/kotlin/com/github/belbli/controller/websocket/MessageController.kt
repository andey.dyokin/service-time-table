package com.github.belbli.controller.websocket

import com.github.belbli.mapper.ChatMessageMapper
import com.github.belbli.model.ChatMessage
import com.github.belbli.model.requests.NewMessageRequest
import com.github.belbli.service.ChatService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import org.springframework.transaction.annotation.Transactional


@Controller
class MessageController @Autowired constructor(
    private val messagingTemplate: SimpMessagingTemplate,
    private val chatMessageService: ChatService,
) {

    @Autowired
    private lateinit var messageMapper: ChatMessageMapper

    @MessageMapping("/chat")
    @Transactional
    fun processMessage(@Payload chatMessage: NewMessageRequest) {

        val newMessage: ChatMessage = chatMessageService.saveMessage(chatMessage)

        newMessage.chat.members.forEach {
            messagingTemplate.convertAndSendToUser(
                it.personInfoId.toString(), "/queue/messages",
                messageMapper.toChatMessageDto(newMessage)
            )
        }
    }
}