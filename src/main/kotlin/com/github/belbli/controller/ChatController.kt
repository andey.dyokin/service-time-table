package com.github.belbli.controller

import com.github.belbli.mapper.ChatMapper
import com.github.belbli.mapper.PersonInfoMapper
import com.github.belbli.model.dto.ChatDto
import com.github.belbli.model.dto.PersonInfoDto
import com.github.belbli.model.requests.ChatCreationRequest
import com.github.belbli.model.requests.ChatInvite
import com.github.belbli.model.requests.ChatRenameRequest
import com.github.belbli.model.requests.LeaveChatRequest
import com.github.belbli.service.ChatService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal
import java.util.UUID

@RestController
@RequestMapping("/api/v1/chats")
class ChatController @Autowired constructor(
    private val chatService: ChatService
) {

    @Autowired
    private lateinit var chatMapper: ChatMapper

    @Autowired
    private lateinit var personInfoMapper: PersonInfoMapper

    @GetMapping
    fun getUserChats(principal: Principal, pageable: Pageable): Page<ChatDto> {
        val userChats = chatService.findUserChats(principal.name, pageable)
        return PageImpl(
            userChats.content.map { chatMapper.toChatDto(it) },
            pageable, userChats.totalElements
        )
    }

    @GetMapping("/{chatId}")
    fun getChatById(@PathVariable(name = "chatId") chatId: UUID, principal: Principal): ChatDto {
        return chatMapper.toChatDto(
            chatService.findChatById(chatId, principal.name)
        )
    }

    @PostMapping
    fun createNewChat(
        @RequestBody chatCreationRequest: ChatCreationRequest,
        principal: Principal
    ): ChatDto {
        return chatMapper.toChatDto(
            chatService.createNewChat(chatCreationRequest, principal.name)
        )
    }

    @PatchMapping("/{chatId}")
    fun inviteMemberToChat(
        @RequestBody invite: ChatInvite,
        @PathVariable chatId: UUID,
        principal: Principal
    ): List<PersonInfoDto> {
        return personInfoMapper.toPersonInfoDtos(
            chatService.inviteUserToChat(invite, chatId, principal.name)
        )
    }

    @PatchMapping("/{chatId}/name")
    fun renameChat(
        @RequestBody renameRequest: ChatRenameRequest,
        @PathVariable chatId: UUID,
        principal: Principal
    ): ChatDto {
        return chatMapper.toChatDto(
            chatService.renameChat(renameRequest, chatId, principal.name)
        )
    }

    @DeleteMapping("/{chatId}")
    fun deleteChat(
        @PathVariable chatId: UUID,
        principal: Principal
    ): Int {
        return chatService.deleteChat(chatId, principal.name)
    }

    @PatchMapping("/{chatId}/leave")
    fun leaveChat(
        @RequestBody leaveChatRequest: LeaveChatRequest,
        @PathVariable chatId: UUID,
        principal: Principal
    ): ChatDto {
        return chatMapper.toChatDto(
            chatService.excludeFromChat(leaveChatRequest, chatId, principal.name)
        )
    }
}