package com.github.belbli.controller

import com.github.belbli.mapper.PersonInfoMapper
import com.github.belbli.model.dto.PersonInfoDto
import com.github.belbli.model.requests.UserSearchRequest
import com.github.belbli.service.PersonInfoService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/users")
class UserController @Autowired constructor(
    private val personInfoService: PersonInfoService
){
    @Autowired
    private lateinit var personInfoMapper: PersonInfoMapper

    @PostMapping
    fun findByEmailOrName(
        @RequestBody userSearchRequest: UserSearchRequest,
        pageable: Pageable
    ): List<PersonInfoDto> {
        return personInfoMapper.toPersonInfoDtos(
            personInfoService.findPersonInfoByEmailOrName(userSearchRequest, pageable)
        )
    }
}