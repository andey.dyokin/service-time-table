package com.github.belbli.controller

import com.github.belbli.mapper.SpecialistMapper
import com.github.belbli.model.dto.SpecialistDto
import com.github.belbli.model.requests.SpecialistDtoWithPassword
import com.github.belbli.model.requests.SpecialistPageRequest
import com.github.belbli.model.response.SpecialistResponse
import com.github.belbli.service.SpecialistService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/api/v1/specialists")
class SpecialistController @Autowired constructor(
        private val specialistMapper: SpecialistMapper,
        private val specialistService: SpecialistService
) {
    @Autowired
    private lateinit var mapper: SpecialistMapper

    @GetMapping("/specialist/{id}")
    fun findSpecialistById(@PathVariable id: UUID): SpecialistResponse {
        return mapper.toSpecialistResponse(
                specialistService.findSpecialist(id)
        )
    }

    @PostMapping("/specialist")
    fun createNewSpecialist(@RequestBody specialistDto: SpecialistDtoWithPassword): SpecialistDto {
        return specialistMapper.toSpecialistDto(
                specialistService.createSpecialistForOrganization(
                        specialistMapper.toSpecialist(specialistDto),
                        specialistDto.organizationId!!
                )
        )
    }

    @PostMapping("/organization")
    fun findSpecialistsFromOrganization(@RequestBody specialistPageRequest: SpecialistPageRequest): Page<SpecialistDto> {
        val specialists = specialistService.findOrganizationSpecialists(
                specialistPageRequest.organizationId, specialistPageRequest.toSpringPageRequest()
        )

        return PageImpl(
                mapper.toSpecialistDtos(specialists.content),
                specialists.pageable,
                specialists.totalElements
        )
    }

    @DeleteMapping("/specialist/{id}")
    fun deleteSpecialist(@PathVariable id: UUID): UUID {
        specialistService.deleteSpecialistById(id)
        return id
    }

    @PutMapping("/specialist")
    fun updateSpecialist(@RequestBody specialistDto: SpecialistDto): SpecialistDto {
        return specialistMapper.toSpecialistDto(
                specialistService.updateSpecialist(specialistDto)
        )
    }
}