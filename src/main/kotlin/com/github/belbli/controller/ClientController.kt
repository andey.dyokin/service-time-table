package com.github.belbli.controller

import com.github.belbli.mapper.ClientMapper
import com.github.belbli.model.dto.ClientDto
import com.github.belbli.service.ClientService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/api/v1/clients")
class ClientController @Autowired constructor(
        private val clientService: ClientService
) {
    @Autowired
    private lateinit var clientMapper: ClientMapper

    @GetMapping("/{id}")
    fun findClientById(@PathVariable id: UUID): ClientDto {
        return clientMapper.toClientDto(
                clientService.findClientById(id)
        )
    }
}