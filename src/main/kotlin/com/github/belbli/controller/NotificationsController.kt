package com.github.belbli.controller

import com.github.belbli.model.Notification
import com.github.belbli.service.NotificationsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal

@RestController
@RequestMapping("/api/v1/notifications")
class NotificationsController @Autowired constructor(
    private val notificationsService: NotificationsService
) {
    @GetMapping
    fun getUserNotifications(principal: Principal): List<Notification> {
        return notificationsService.findUserNotifications(principal.name)
    }
}