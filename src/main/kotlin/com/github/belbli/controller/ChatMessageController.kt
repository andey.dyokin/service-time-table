package com.github.belbli.controller

import com.github.belbli.mapper.ChatMessageMapper
import com.github.belbli.model.dto.ChatMessageDto
import com.github.belbli.model.requests.MessageUpdateRequest
import com.github.belbli.service.ChatMessageService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal
import java.util.UUID

@RestController
@RequestMapping("/api/v1/messages")
class ChatMessageController @Autowired constructor(
    private val messageService: ChatMessageService
) {

    @Autowired
    private lateinit var messageMapper: ChatMessageMapper

    @DeleteMapping("/{messageId}")
    fun deleteMessage(
        @PathVariable("messageId") messageId: UUID,
        principal: Principal
    ): UUID {
        return messageService.deleteMessageById(messageId, principal.name)
    }

    @PatchMapping("/{messageId}")
    fun updateMessageText(
        @PathVariable("messageId") messageId: UUID,
        @RequestBody messageUpdateRequest: MessageUpdateRequest,
        principal: Principal
    ): ChatMessageDto {
        return messageMapper.toChatMessageDto(
            messageService.updateMessage(messageId, messageUpdateRequest, principal.name)
        )
    }
}