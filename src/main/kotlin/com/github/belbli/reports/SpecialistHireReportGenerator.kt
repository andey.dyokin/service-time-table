package com.github.belbli.reports

import com.github.belbli.model.Specialist
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.stereotype.Component
import java.io.ByteArrayOutputStream
import java.io.FileOutputStream


@Component
class SpecialistHireReportGenerator {

    private val headers = listOf(
            "email", "firstname", "lastname", "hire date", "role"
    )

    fun writeSpecialistReport(specialists: List<Specialist>): ByteArray {
        val workbook: Workbook = XSSFWorkbook()
        val sheet: Sheet = workbook.createSheet("Persons")

        val headerRow: Row = sheet.createRow(0)

        writeRow(headerRow, headers)

        specialists.forEach {
            writeRow(sheet.createRow(sheet.lastRowNum+1), extractReportedFields(it))
        }

        val outputStream = FileOutputStream("report.xlsx")
        workbook.write(outputStream)

        val out = ByteArrayOutputStream()
        workbook.write(out)

        out.close()
        workbook.close()

        return out.toByteArray()
    }

    private fun extractReportedFields(specialist: Specialist): List<String> {
        return listOf(
                specialist.personInfo.email,
                specialist.personInfo.firstname,
                specialist.personInfo.lastname,
                specialist.personInfo.createDate.toString(),
                specialist.role
        )
    }

    private fun writeRow(row: Row, values: List<String>) {
        values.forEach { createCell(row, it) }
    }

    private fun createCell(row: Row, data: String): Cell {
        val cell = row.createCell(getLastCellNum(row))
        cell.setCellValue(data)
        return cell
    }

    private fun getLastCellNum(row: Row): Int {
        return if(row.lastCellNum.toInt() == -1) 0 else row.lastCellNum.toInt()
    }
}