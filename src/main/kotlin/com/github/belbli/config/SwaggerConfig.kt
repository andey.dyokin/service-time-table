package com.github.belbli.config

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.swagger.models.auth.In
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.http.HttpHeaders
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.ApiKey
import springfox.documentation.service.AuthorizationScope
import springfox.documentation.service.SecurityReference
import springfox.documentation.service.SecurityScheme
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spi.service.contexts.SecurityContext
import springfox.documentation.spring.web.plugins.Docket


@Configuration
class SwaggerConfig {
    private val INTERNAL_SECURITY_SCHEME_NAME = "Service Timetable"

    @Bean
    fun apiDocs(): Docket? {
        return Docket(DocumentationType.SWAGGER_2)
                .securitySchemes(listOf(internalSecurityScheme()))
                .securityContexts(listOf(appSecurityContext()))
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.github.belbli.controller"))
                .paths(PathSelectors.regex("/api.*"))
                .build()
                .apiInfo(apiEndPointsInfo())
                .enable(true)
                .select()
                .build()
    }

    private fun internalSecurityScheme(): SecurityScheme {
        return ApiKey(INTERNAL_SECURITY_SCHEME_NAME, HttpHeaders.AUTHORIZATION, In.HEADER.toValue())
    }

    private fun appSecurityContext(): SecurityContext? {
        return SecurityContext.builder()
                .securityReferences(listOf(internalSecurityReference()))
                .build()
    }

    private fun internalSecurityReference(): SecurityReference {
        val authorizationScopes: Array<AuthorizationScope?> = arrayOfNulls(1)
        authorizationScopes[0] = AuthorizationScope("global", "accessEverything")
        return SecurityReference(INTERNAL_SECURITY_SCHEME_NAME, authorizationScopes)
    }

    private fun apiEndPointsInfo(): ApiInfo? {
        return ApiInfoBuilder()
                .title("Spring Boot REST API")
                .description("TEAM BOARD REST API")
                .build()
    }

    @Bean
    @Primary
    fun objectMapper() = ObjectMapper().apply {
        registerModule(KotlinModule())
        configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, false)
        configure(DeserializationFeature.FAIL_ON_NULL_CREATOR_PROPERTIES, false)
    }

}
