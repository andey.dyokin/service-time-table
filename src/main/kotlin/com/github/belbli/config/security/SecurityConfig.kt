package com.github.belbli.config.security

import com.github.belbli.model.UserRole
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.HttpStatusEntryPoint
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter


@Configuration
class SecurityConfig(@Autowired private val jwtFilter: JwtFilter) : WebSecurityConfigurerAdapter() {

    companion object {
        private val PERMIT_ALL_ENDPOINTS = arrayOf(
            "/swagger-ui/**",
            "/register",
            "/api/v1/auth/**"
        )
        private val ONLY_GET_ENDPOINTS = arrayOf(
            "/api/v1/client/**",
            "/api/v1/specialists/**",
            "/api/v1/organizations/**",
            "/api/v1/service-requests/**"
        )
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http
            .httpBasic().disable()
            .cors().and()
            .csrf().disable()
            .authorizeRequests()
            .antMatchers("/api/v1/root-users/**").hasAuthority(UserRole.ROOT_USER.name)
            .antMatchers(HttpMethod.GET, *ONLY_GET_ENDPOINTS).permitAll()
            .antMatchers(*ONLY_GET_ENDPOINTS).authenticated()
            .antMatchers(*PERMIT_ALL_ENDPOINTS).permitAll()
            .and()
            .logout().permitAll()
            .and()
            .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter::class.java)
    }
}
