package com.github.belbli.config.security

import com.github.belbli.repository.PersonInfoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service


@Service
class JwtUserDetailsService : UserDetailsService {
    @Autowired
    private lateinit var personInfoRepository: PersonInfoRepository

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(email: String): UserDetails {
        val personInfo = personInfoRepository.findByEmail(email)
        return if (personInfo != null) {
            User(personInfo.email, personInfo.password, arrayListOf(SimpleGrantedAuthority(personInfo.userRole.name)))
        } else {
            throw UsernameNotFoundException("User with email = $email not found")
        }
    }
}