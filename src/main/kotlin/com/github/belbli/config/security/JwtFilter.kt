package com.github.belbli.config.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils.hasText
import org.springframework.web.filter.GenericFilterBean
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest


@Component
class JwtFilter @Autowired constructor(
        private val jwtProvider: JwtProvider,
        private val service: JwtUserDetailsService) : GenericFilterBean() {

    @Throws(IOException::class, ServletException::class)
    override fun doFilter(servletRequest: ServletRequest,
                          servletResponse: ServletResponse,
                          filterChain: FilterChain
    ) {
        var token = getTokenFromRequest(servletRequest as HttpServletRequest)
        if (token == null) {
            val auth = servletRequest.getParameter("auth")
            if (auth != null && auth.isNotBlank()) {
                token = servletRequest.getParameter("auth")
            }
        }
        if (token != null && jwtProvider.validateToken(token)) {
            val email = jwtProvider.getLoginFromToken(token)
            val userDetails = service.loadUserByUsername(email)
            val auth = UsernamePasswordAuthenticationToken(
                    userDetails,
                    null,
                    userDetails.authorities
            )
            SecurityContextHolder.getContext().authentication = auth
        }
        filterChain.doFilter(servletRequest, servletResponse)
    }

    private fun getTokenFromRequest(request: HttpServletRequest): String? {
        val bearer = request.getHeader(HttpHeaders.AUTHORIZATION)
        return if (hasText(bearer) && bearer.startsWith(BEARER, true)) {
            bearer.substring(7)
        } else null
    }

    companion object {
        const val AUTHORIZATION = "Authorization"
        const val BEARER = "Bearer "
    }
}
