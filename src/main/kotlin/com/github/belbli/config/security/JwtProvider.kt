package com.github.belbli.config.security

import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.MalformedJwtException
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.SignatureException
import io.jsonwebtoken.UnsupportedJwtException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.time.LocalDate
import java.time.ZoneId
import java.util.*


@Component
class JwtProvider {
    val logger = LoggerFactory.getLogger(this::class.java)

    @Value("$(jwt.secret)")
    private val jwtSecret: String? = null
    fun generateToken(login: String): String {
        val date: Date = Date.from(LocalDate.now().plusDays(15).atStartOfDay(ZoneId.systemDefault()).toInstant())
        return Jwts.builder()
                .setSubject(login)
                .setExpiration(date)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact()
    }

    fun validateToken(token: String?): Boolean {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token)
            return true
        } catch (expEx: ExpiredJwtException) {
            logger.warn("Token expired")
        } catch (unsEx: UnsupportedJwtException) {
            logger.warn("Unsupported jwt")
        } catch (mjEx: MalformedJwtException) {
            logger.warn("Malformed jwt")
        } catch (sEx: SignatureException) {
            logger.warn("Invalid signature")
        } catch (e: Exception) {
            logger.warn("invalid token")
        }
        return false
    }

    fun getLoginFromToken(token: String?): String {
        val claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).body
        return claims.subject
    }
}