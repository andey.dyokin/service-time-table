package com.github.belbli.service

import com.github.belbli.exceptions.NotFoundException
import com.github.belbli.mapper.ClientMapper
import com.github.belbli.model.Client
import com.github.belbli.model.ServiceRequest
import com.github.belbli.model.UserRole
import com.github.belbli.model.dto.ClientDto
import com.github.belbli.repository.ClientRepository
import com.github.belbli.repository.PersonInfoRepository
import com.github.belbli.repository.ServiceRequestRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.server.ResponseStatusException
import java.util.*

@Service
class ClientService @Autowired constructor(
        private val clientRepository: ClientRepository,
        private val personInfoRepository: PersonInfoRepository,
        private val serviceRequestRepository: ServiceRequestRepository
) {
    @Autowired
    private lateinit var clientMapper: ClientMapper
    @Autowired
    private lateinit var encoder: PasswordEncoder

    @Transactional
    fun createClient(client: Client): Client {
        val personInfo = personInfoRepository.findByEmail(client.personInfo.email)
        return if(personInfo == null) {
            clientRepository.save(
                    client.copy(
                            personInfo = client.personInfo.copy(
                                    password = encoder.encode(client.personInfo.password),
                                    userRole = UserRole.CLIENT
                            )
                    )
            )
        } else {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "email has already been taken")
        }
    }

    @Transactional
    fun findClientById(clientId: UUID): Client {
        return clientRepository.findByIdOrNull(clientId)
            ?: throw NotFoundException("client with id = $clientId not found")
    }

    fun findClientRequests(clientId: UUID, pageable: Pageable): Page<ServiceRequest> {
        return serviceRequestRepository
            .findAllByCustomerClientId(clientId, pageable)
    }
}