package com.github.belbli.service

import com.github.belbli.exceptions.NotFoundException
import com.github.belbli.mapper.AddressMapper
import com.github.belbli.model.Meeting
import com.github.belbli.model.MeetingStatus
import com.github.belbli.model.TimeTable
import com.github.belbli.model.requests.NewMeetingRequest
import com.github.belbli.repository.ClientRepository
import com.github.belbli.repository.MeetingRepository
import com.github.belbli.repository.PersonInfoRepository
import com.github.belbli.repository.SpecialistRepository
import com.github.belbli.repository.TimeTableRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.server.ResponseStatusException
import java.util.*

@Service
class TimetableService @Autowired constructor(
    private val clientRepository: ClientRepository,
    private val meetingRepository: MeetingRepository,
    private val timeTableRepository: TimeTableRepository,
    private val personInfoRepository: PersonInfoRepository,
    private val specialistRepository: SpecialistRepository
) {
    @Autowired
    private lateinit var addressMapper: AddressMapper

    fun createTimetable(): TimeTable {
        return timeTableRepository.save(TimeTable(null, mutableListOf()))
    }

    fun getTimetableById(timetableId: UUID): TimeTable {
        return timeTableRepository.findByIdOrNull(timetableId)
            ?: throw NotFoundException("timetable with id = $timetableId not found")
    }

    @Transactional
    fun addMeeting(newMeetingRequest: NewMeetingRequest): Meeting {
        val organizer = specialistRepository.findByIdOrNull(newMeetingRequest.organizerId)
            ?: throw NotFoundException("timetable with id = $newMeetingRequest.timetableId not found")

        val allParticipants = clientRepository.findAllByPersonInfoEmailIn(newMeetingRequest.participantEmails)
        if (allParticipants.count() != newMeetingRequest.participantEmails.size) {
            throw NotFoundException("some of the participants are not exist")
        }

        val newMeeting = meetingRepository.save(
            Meeting(
                meetingId = null,
                timeTableId = organizer.timeTable!!.timeTableId,
                startDate = newMeetingRequest.startDate,
                endDate = newMeetingRequest.endDate,
                address = addressMapper.toAddress(newMeetingRequest.address),
                organizer = organizer,
                participants = allParticipants,
                status = MeetingStatus.PLANNED
            )
        )
        val updatedParticipants = allParticipants.map {
            it.meetings.add(newMeeting)
            it
        }
        clientRepository.saveAll(updatedParticipants)

        val timeTable = organizer.timeTable
        timeTable.meetings.add(newMeeting)
        timeTableRepository.save(timeTable)

        return newMeeting
    }

    fun saveAll(timeTables: List<TimeTable>) {
        timeTableRepository.saveAll(timeTables)
    }

    fun getTimetableByUserId(userId: UUID): TimeTable {
        val specialist = specialistRepository.findByIdOrNull(userId)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "timetable for user id = $userId not found")
        return specialist.timeTable!!
    }
}
