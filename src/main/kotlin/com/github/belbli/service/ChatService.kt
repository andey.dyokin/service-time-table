package com.github.belbli.service

import com.github.belbli.mapper.PersonInfoMapper
import com.github.belbli.model.Chat
import com.github.belbli.model.ChatMessage
import com.github.belbli.model.Notification
import com.github.belbli.model.NotificationEventType
import com.github.belbli.model.PersonInfo
import com.github.belbli.model.requests.ChatCreationRequest
import com.github.belbli.model.requests.ChatInvite
import com.github.belbli.model.requests.ChatRenameRequest
import com.github.belbli.model.requests.LeaveChatRequest
import com.github.belbli.model.requests.NewMessageRequest
import com.github.belbli.repository.ChatMessagesRepository
import com.github.belbli.repository.ChatRepository
import com.github.belbli.repository.NotificationRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.server.ResponseStatusException
import java.util.Date
import java.util.UUID

@Service
class ChatService @Autowired constructor(
    private val chatRepository: ChatRepository,
    private val chatMessagesRepository: ChatMessagesRepository,
    private val personInfoService: PersonInfoService,
    private val notificationsService: NotificationsService,
    private val notificationRepository: NotificationRepository
) {
    @Autowired
    private lateinit var personInfoMapper: PersonInfoMapper

    @Transactional
    fun saveMessage(message: NewMessageRequest): ChatMessage {
        val chat = chatRepository.findByIdOrNull(message.chatId)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "chat with id = ${message.chatId} not found.")
        val sender = personInfoService.findPersonInfoById(message.senderId)
        val newMessage = chatMessagesRepository.save(
            ChatMessage(
                null,
                message.message,
                sender = sender,
                chat = chat,
                Date(), Date()
            )
        )
        chat.messages.add(newMessage)
        chatRepository.save(chat)

        return newMessage
    }

    fun createNewChat(chatCreationRequest: ChatCreationRequest, requesterEmail: String): Chat {
        val chatMembers = personInfoService.findAllByEmails(chatCreationRequest.memberEmails).toMutableList()
        val chatCreator = personInfoService.findPersonInfoByEmailWithException(requesterEmail)
        chatMembers.add(chatCreator)

        return chatRepository.save(
            Chat(
                name = chatCreationRequest.chatName,
                members = chatMembers,
                chatCreator = chatCreator,
                messages = mutableListOf(),
                chatId = null,
                chatImage = chatCreationRequest.chatImage
            )
        )
    }

    fun findUserChats(requesterEmail: String, pageable: Pageable): Page<Chat> {
        val chatCreator = personInfoService.findPersonInfoByEmailWithException(requesterEmail)
        return chatRepository.findUserChatsByMembersPersonInfoId(
            chatCreator.personInfoId!!, pageable
        )

    }

    fun findChatById(chatId: UUID, requesterEmail: String): Chat {
        val chat = chatRepository.findByIdOrNull(chatId)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "chat with id $chatId not found")

        if (chat.chatCreator.email == requesterEmail ||
            chat.members.find { it.email == requesterEmail } != null) {
            return chat
        }

        throw ResponseStatusException(HttpStatus.FORBIDDEN, "user is not allowed to get the requested data.")
    }

    fun inviteUserToChat(invite: ChatInvite, chatId: UUID, requestSenderEmail: String): List<PersonInfo> {
        val chat = findChatById(chatId, requestSenderEmail)
        val emailsToInvite = invite.userEmails.filter { email -> chat.members.find { it.email == email } == null }
        if (emailsToInvite.isNotEmpty()) {
            val personInfos = personInfoService.findAllByEmails(emailsToInvite)
            chat.members.addAll(personInfos)
            chatRepository.save(chat)

            personInfos.forEach {
                val notification = notificationRepository.save(
                    Notification(
                        null,
                        "You've been added to the ${chat.name} chat.",
                        NotificationEventType.CHAT_INVITATION,
                        redirectId = chat.chatId!!,
                        recipientId = it.personInfoId!!,
                        Date()
                    )
                )
                notificationsService.sendNotification(notification)
            }

            return personInfos
        }
        throw ResponseStatusException(HttpStatus.BAD_REQUEST, "user already is in the chat.")
    }

    fun renameChat(renameRequest: ChatRenameRequest, chatId: UUID, requesterEmail: String): Chat {
        val chat = findChatById(chatId, requesterEmail)
        return chatRepository.save(
            chat.copy(name = renameRequest.newName)
        )
    }

    fun deleteChat(chatId: UUID, requesterEmail: String): Int {
        val chat = findChatById(chatId, requesterEmail)

        if(chat.chatCreator.email == requesterEmail) {
            chatRepository.deleteById(chatId)
            return 1
        }
        throw ResponseStatusException(HttpStatus.UNAUTHORIZED, "only chat creator can delete chat.")
    }

    fun excludeFromChat(leaveChatRequest: LeaveChatRequest, chatId: UUID, requesterEmail: String): Chat {
        val chat = findChatById(chatId, requesterEmail)
        if (leaveChatRequest.userEmail == requesterEmail || chat.chatCreator.email == requesterEmail) {
            chat.members.removeIf { it.email == leaveChatRequest.userEmail }
            return chatRepository.save(chat)
        }

        throw ResponseStatusException(HttpStatus.UNAUTHORIZED, "user can't kick other user from chat.")
    }
}