package com.github.belbli.service

import com.github.belbli.mapper.AddressMapper
import com.github.belbli.model.Specialist
import com.github.belbli.model.UserRole
import com.github.belbli.model.dto.SpecialistDto
import com.github.belbli.repository.OrganizationRepository
import com.github.belbli.repository.PersonInfoRepository
import com.github.belbli.repository.SpecialistRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.util.*

@Service
class SpecialistService @Autowired constructor(
        private val addressMapper: AddressMapper,
        private val specialistRepository: SpecialistRepository,
        private val timetableService: TimetableService,
        private val rootUserService: RootUserService,
        private val personInfoRepository: PersonInfoRepository,
        private val organizationRepository: OrganizationRepository
) {
    @Autowired
    private lateinit var encoder: PasswordEncoder

    fun findSpecialist(id: UUID): Specialist {
        return specialistRepository.findByIdOrNull(id)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "user with id = $id not found")
    }

    fun createSpecialistForOrganization(specialist: Specialist, organizationId: UUID): Specialist {
        val organization = organizationRepository.findByIdOrNull(organizationId)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "organization doesn't exist")
        return createSpecialist(specialist.copy(organization = organization))
    }

    fun createSpecialist(specialist: Specialist): Specialist {
        val info = personInfoRepository.findByEmail(specialist.personInfo.email)
        return if (info == null) {
            specialistRepository.save(
                    specialist.copy(
                            timeTable = timetableService.createTimetable(),
                            personInfo = specialist.personInfo.copy(
                                    password = encoder.encode(specialist.personInfo.password),
                                    userRole = UserRole.SPECIALIST
                            )
                    )
            )
        } else {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "email has already been taken")
        }
    }

    fun findOrganizationSpecialists(organizationId: UUID, pageRequest: PageRequest): Page<Specialist> {
        return specialistRepository.findSpecialistsByOrganizationOrganizationId(
                organizationId, pageRequest
        )
    }

    fun generateHiredSpecialistsReport(organizationId: UUID, start: Date, end: Date): List<Specialist> {
        return findSpecialistsHiredBetweenDates(organizationId, start, end)
    }

    fun findSpecialistsHiredBetweenDates(organizationId: UUID, start: Date, end: Date): List<Specialist> {
        return specialistRepository
                .findByOrganizationOrganizationIdAndPersonInfoCreateDateBetween(organizationId, start, end)
    }

    fun deleteSpecialistById(id: UUID) {
        val rootUser = rootUserService
                .findRootUserByEmail(SecurityContextHolder.getContext().authentication.name)

        rootUser.organization?.specialists?.filter { it.specialistId == id }?.firstOrNull()
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "deleting specialist doesn't belong to your organization")

        return specialistRepository.deleteById(id)
    }

    fun updateSpecialist(specialistDto: SpecialistDto): Specialist {
        val specialist = specialistRepository.findByIdOrNull(specialistDto.specialistId)
                ?:throw ResponseStatusException(HttpStatus.BAD_REQUEST, "specialist with id = ${specialistDto.specialistId} doesn't exist")

        return specialistRepository.save(
                specialist.copy(
                        workAddress = addressMapper.toAddress(specialistDto.workAddress),
                        role = specialistDto.role,
                        experienceInYears = specialistDto.experienceInYears,
                        personInfo = specialist.personInfo.copy(
                                firstname = specialistDto.personInfo.firstname,
                                lastname = specialistDto.personInfo.lastname,
                                email = specialistDto.personInfo.email,
                                image = specialistDto.personInfo.image,
                        )
                )
        )
    }
}