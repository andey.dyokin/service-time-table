package com.github.belbli.service

import com.github.belbli.model.RootUser
import com.github.belbli.model.UserRole
import com.github.belbli.repository.RootUserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.util.*

@Service
class RootUserService @Autowired constructor(
        private val personInfoService: PersonInfoService,
        private val rootUserRepository: RootUserRepository
) {
    @Autowired
    private lateinit var encoder: PasswordEncoder

    fun findRootUserById(rootUserId: UUID): RootUser {
        return rootUserRepository.findByIdOrNull(rootUserId)
                ?: throw ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        "root user with id $rootUserId not found"
                )
    }

    fun findRootUserByEmail(email: String): RootUser {
        return rootUserRepository.findByPersonInfoEmail(email)
                ?: throw ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        "root user with email $email not found"
                )
    }

    fun createRootUser(rootUser: RootUser): RootUser {
        return if (personInfoService.findPersonInfoByEmail(rootUser.personInfo.email) == null) {
            rootUserRepository.save(
                    rootUser.copy(
                            personInfo = rootUser.personInfo.copy(
                                    userRole = UserRole.ROOT_USER,
                                    password = encoder.encode(rootUser.personInfo.password)
                            )
                    )
            )
        } else {
            throw ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "can't create root user. user email = ${rootUser.personInfo.email} has been already used"
            )
        }
    }
}