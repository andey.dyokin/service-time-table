package com.github.belbli.service

import com.github.belbli.model.Specialist
import com.github.belbli.model.dto.HiredChartData
import com.github.belbli.reports.SpecialistHireReportGenerator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import java.io.ByteArrayOutputStream
import java.util.*

@Service
class ReportGeneratorService @Autowired constructor(
        private val rootUserService: RootUserService,
        private val specialistService: SpecialistService,
        private val specialistHireReportGenerator: SpecialistHireReportGenerator
) {

    fun generateHiredSpecialistsReport(start: Date, end: Date): HiredChartData {
        val specialists = getSpecialistForReport(start, end)
        val byRole = specialists.groupBy { it.role }
        val byCountry = specialists.groupBy { it.workAddress.country }
        val byCity = specialists.groupBy { it.workAddress.city }
        val byExperience = specialists.groupBy { it.experienceInYears }
        return HiredChartData(
                byRole.mapValues { it.value.size },
                byCountry.mapValues { it.value.size },
                byCity.mapValues { it.value.size },
                byExperience.mapValues { it.value.size },
        )
    }

    private fun getSpecialistForReport(start: Date, end: Date): List<Specialist> {
        val rootUser = rootUserService
                .findRootUserByEmail(SecurityContextHolder.getContext().authentication.name)

        return specialistService.generateHiredSpecialistsReport(
                rootUser.organization?.organizationId!!, start, end
        )
    }

    fun generateHiredSpecialistsReportXlxs(start: Date, end: Date): ByteArray {
        return specialistHireReportGenerator.writeSpecialistReport(getSpecialistForReport(start, end))
    }
}