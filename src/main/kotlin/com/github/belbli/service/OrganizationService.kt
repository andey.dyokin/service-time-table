package com.github.belbli.service

import com.github.belbli.model.Organization
import com.github.belbli.model.ServiceRequest
import com.github.belbli.model.UserRole
import com.github.belbli.repository.AddressRepository
import com.github.belbli.repository.OrganizationDomainRepository
import com.github.belbli.repository.OrganizationRepository
import com.github.belbli.repository.PersonInfoRepository
import com.github.belbli.repository.ServiceRequestRepository
import com.github.belbli.repository.SpecialistRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.server.ResponseStatusException
import java.util.*

@Service
class OrganizationService @Autowired constructor(
    private val rootUserService: RootUserService,
    private val timetableService: TimetableService,
    private val addressRepository: AddressRepository,
    private val personInfoRepository: PersonInfoRepository,
    private val specialistRepository: SpecialistRepository,
    private val organizationRepository: OrganizationRepository,
    private val organizationDomainRepository: OrganizationDomainRepository,
    private val serviceRequestRepository: ServiceRequestRepository,
) {

    @Autowired
    private lateinit var encoder: PasswordEncoder

    fun findOrganizations(pageRequest: PageRequest): List<Organization?> {
        return organizationRepository.findAll(pageRequest).content.toList()
    }

    fun findOrganizationsByName(organizationName: String, page: Pageable): Page<Organization> {
        return organizationRepository.findByNameContainingIgnoreCase(organizationName, page)
    }

    fun findOrganizationRequests(organizationId: UUID, page: Pageable): Page<ServiceRequest> {
        return serviceRequestRepository
            .findServiceRequestsByExecutorOrganizationOrganizationId(organizationId, page)
    }

    @Transactional
    fun createOrganization(organization: Organization): Organization {
        val rootUser = rootUserService
                .findRootUserByEmail(SecurityContextHolder.getContext().authentication.name)

        val organizationDomain = organization.organizationDomain

        var findByDomainName = organizationDomainRepository.findByDomainName(organizationDomain.domainName)
        if (findByDomainName == null)
            findByDomainName = organizationDomainRepository.save(organizationDomain)

        val savedOrganization = organizationRepository.save(
            organization.copy(
                rootUser = rootUser,
                organizationDomain = findByDomainName,
            )
        )

        val specialists = organization.specialists.map {
            it.copy(
                organization = savedOrganization,
                timeTable = timetableService.createTimetable(),
                personInfo = it.personInfo.copy(
                    password = encoder.encode(it.personInfo.password),
                    userRole = UserRole.SPECIALIST
                )
            )
        }
        val savedSpecialists = specialistRepository.saveAll(specialists)

        return savedOrganization.copy(
            specialists = savedSpecialists.toList()
        )
    }

    fun findOrganizationById(id: UUID): Organization {
        return organizationRepository.findByIdOrNull(id)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "organization not found")
    }
}