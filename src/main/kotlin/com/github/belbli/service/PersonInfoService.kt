package com.github.belbli.service

import com.github.belbli.exceptions.NotFoundException
import com.github.belbli.mapper.ClientMapper
import com.github.belbli.mapper.RootUserMapper
import com.github.belbli.mapper.SpecialistMapper
import com.github.belbli.model.Client
import com.github.belbli.model.PersonInfo
import com.github.belbli.model.RootUser
import com.github.belbli.model.Specialist
import com.github.belbli.model.UserRole
import com.github.belbli.model.dto.CredentialsDto
import com.github.belbli.model.requests.UserSearchRequest
import com.github.belbli.repository.ClientRepository
import com.github.belbli.repository.PersonInfoRepository
import com.github.belbli.repository.RootUserRepository
import com.github.belbli.repository.SpecialistRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.util.*

@Service
class PersonInfoService @Autowired constructor(
        private val personInfoRepository: PersonInfoRepository,
        private val rootUserRepository: RootUserRepository,
        private val specialistRepository: SpecialistRepository,
        private val clintRepository: ClientRepository,
        private val clientMapper: ClientMapper,
        private val rootUserMapper: RootUserMapper,
        private val specialistMapper: SpecialistMapper,
) {
    @Autowired
    private lateinit var encoder: PasswordEncoder

    fun findPersonInfoByCredentials(credentials: CredentialsDto): PersonInfo {
        val findByEmail = personInfoRepository.findByEmail(credentials.email)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "email doesn't exist")
        return if (encoder.matches(credentials.password, findByEmail.password)) {
            findByEmail
        } else {
            throw ResponseStatusException(HttpStatus.UNAUTHORIZED, "wrong password")
        }
    }

    fun findAllByPersonInfoIds(personInfoIds: List<UUID>?): List<PersonInfo> {
        return if (personInfoIds == null || personInfoIds.isEmpty()) {
            emptyList()
        } else {
            personInfoRepository.findAllByPersonInfoIdIn(personInfoIds)
        }
    }

    fun findAllByEmails(emails: List<String>?): List<PersonInfo> {
        return if (emails == null || emails.isEmpty()) {
            emptyList()
        } else {
            personInfoRepository.findAllByEmailIn(emails)
        }
    }

    fun findPersonInfoByEmail(email: String): PersonInfo? {
        return personInfoRepository.findByEmail(email)
    }

    fun findPersonInfoByEmailWithException(email: String): PersonInfo {
        return personInfoRepository.findByEmail(email)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "user with email $email not found.")
    }

    fun findUserCredentials(credentialsDto: CredentialsDto): Any {
        val personInfo = findPersonInfoByCredentials(credentialsDto)

        return getConcreteUserDto(personInfo)
    }

    fun findUserByEmail(email: String): Any {
        val personInfo = findPersonInfoByEmail(email)
            ?: throw NotFoundException("user with email = $email not found.")
        return getConcreteUser(personInfo)
    }

    private fun getConcreteUser(personInfo: PersonInfo): Any {
        return when (personInfo.userRole) {
            UserRole.ROOT_USER -> findRootUserByPersonInfoId(personInfo.personInfoId!!)
            UserRole.SPECIALIST -> findSpecialistPersonInfoId(personInfo.personInfoId!!)
            UserRole.CLIENT -> findClientByPersonInfoId(personInfo.personInfoId!!)
            else -> {
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Unknown user role: ${personInfo.userRole}")
            }
        }
    }

    private fun getConcreteUserDto(personInfo: PersonInfo): Any {
        return when (personInfo.userRole) {
            UserRole.ROOT_USER -> rootUserMapper.toRootUserDto(findRootUserByPersonInfoId(personInfo.personInfoId!!))
            UserRole.SPECIALIST -> specialistMapper.toSpecialistDto(findSpecialistPersonInfoId(personInfo.personInfoId!!))
            UserRole.CLIENT -> clientMapper.toClientDto(findClientByPersonInfoId(personInfo.personInfoId!!))
            else -> {
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Unknown user role: ${personInfo.userRole}")
            }
        }
    }

    fun findRootUserByPersonInfoId(personInfoId: UUID): RootUser {
        return rootUserRepository.findByPersonInfoPersonInfoId(personInfoId)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "root user with id $personInfoId doesn't exist")
    }

    fun findSpecialistPersonInfoId(personInfoId: UUID): Specialist {
        return specialistRepository.findByPersonInfoPersonInfoId(personInfoId)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "specialist user with id $personInfoId doesn't exist")
    }

    fun findClientByPersonInfoId(personInfoId: UUID): Client {
        return clintRepository.findByPersonInfoPersonInfoId(personInfoId)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "client user with id $personInfoId doesn't exist")
    }

    fun updatePersonInfo(personInfo: PersonInfo): PersonInfo {
        return personInfoRepository.save(personInfo)
    }

    fun findPersonInfoById(id: UUID): PersonInfo {
        return personInfoRepository.findByIdOrNull(id)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "user with id $id")
    }

    fun findPersonInfoByEmailOrName(userSearchRequest: UserSearchRequest, pageable: Pageable): List<PersonInfo> {
        return personInfoRepository
            .findByEmailContainsOrFirstnameContainsOrLastnameContains(
                userSearchRequest.searchedPart,
                userSearchRequest.searchedPart,
                userSearchRequest.searchedPart,
                pageable
            ).content
    }

}