package com.github.belbli.service

import com.github.belbli.model.Notification
import com.github.belbli.model.NotificationEventType
import com.github.belbli.model.ServiceRequest
import com.github.belbli.model.ServiceRequestComment
import com.github.belbli.model.UserRole
import com.github.belbli.model.requests.ServiceRequestCommentRequest
import com.github.belbli.repository.ServiceRequestCommentRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.security.Principal
import java.util.*
import kotlin.math.min

@Service
class ServiceRequestCommentService @Autowired constructor(
    private val personInfoService: PersonInfoService,
    private val notificationsService: NotificationsService,
    private val serviceRequestService: ServiceRequestService,
    private val serviceRequestCommentRepository: ServiceRequestCommentRepository
) {

    fun saveRequestComment(
        serviceRequestId: UUID,
        principal: Principal,
        commentRequest: ServiceRequestCommentRequest
    ): ServiceRequestComment{
        val serviceRequest = serviceRequestService.findServiceRequestById(serviceRequestId)
        val commenter = personInfoService.findPersonInfoByEmail(principal.name)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "user with email ${principal.name} not found")

        val serviceRequestComment = ServiceRequestComment(
            commentId = null,
            message = commentRequest.comment,
            serviceRequest = serviceRequest,
            commenter = commenter,
        )
        serviceRequestCommentRepository.save(serviceRequestComment)
        val requestComment = commentRequest.comment
        getRecipientId(serviceRequestComment, serviceRequest).forEach {
            val notification = notificationsService.saveNotification(
                Notification(
                    null,
                    "New comment in service request: ${requestComment.substring(0, min(32, requestComment.length))}",
                    NotificationEventType.SERVICE_REQUEST_NEW_COMMENT,
                    serviceRequestComment.serviceRequest.requestId!!,
                    it,
                    Date()
                )
            )
            notificationsService.sendNotification(notification)
        }

        serviceRequest.serviceRequestComments?.add(
            serviceRequestComment
        )
        serviceRequestService.updateServiceRequest(serviceRequest)

        return serviceRequestComment
    }

    private fun getRecipientId(serviceRequestComment: ServiceRequestComment, serviceRequest: ServiceRequest): List<UUID> {
        val ids = mutableListOf<UUID>()
        val commenter = serviceRequestComment.commenter
        if (serviceRequest.executor.personInfo.personInfoId != commenter.personInfoId) {
            ids.add(serviceRequest.executor.personInfo.personInfoId!!)
        }
        if (serviceRequest.customer.personInfo.personInfoId != commenter.personInfoId) {
            ids.add(serviceRequest.customer.personInfo.personInfoId!!)
        }
        return ids
    }
}