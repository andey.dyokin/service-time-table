package com.github.belbli.service

import com.github.belbli.model.ChatMessage
import com.github.belbli.model.dto.ChatMessageDto
import com.github.belbli.model.requests.MessageUpdateRequest
import com.github.belbli.repository.ChatMessagesRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.util.*

@Service
class ChatMessageService @Autowired constructor(
    private val personInfoService: PersonInfoService,
    private val messagesRepository: ChatMessagesRepository
) {
    fun deleteMessageById(messageId: UUID, requestSenderEmail: String): UUID {
        if (isAuthorizedAction(messageId, requestSenderEmail)) {
            messagesRepository.deleteById(messageId)
            return messageId
        }
        throw ResponseStatusException(HttpStatus.FORBIDDEN, "user is not authorized to perform the action.")
    }


    fun findMessageById(messageId: UUID): ChatMessage {
        return messagesRepository.findByIdOrNull(messageId)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "message with id $messageId doesn't exist.")
    }

    fun updateMessage(messageId: UUID, messageUpdateRequest: MessageUpdateRequest, requestSenderEmail: String): ChatMessage {
        val messageToUpdate = findMessageById(messageId)
        val requestSender = personInfoService.findPersonInfoByEmailWithException(requestSenderEmail)
        if(messageToUpdate.sender == requestSender) {
            return messagesRepository.save(
                messageToUpdate.copy(
                    text = messageUpdateRequest.newMessage
                )
            )
        }
        throw ResponseStatusException(HttpStatus.FORBIDDEN, "user is not authorized to perform the action.")
    }

    fun isAuthorizedAction(messageId: UUID, requestSenderEmail: String): Boolean {
        val requestSender = personInfoService.findPersonInfoByEmailWithException(requestSenderEmail)
        val messageToDelete = findMessageById(messageId)

        return messageToDelete.sender == requestSender
    }
}