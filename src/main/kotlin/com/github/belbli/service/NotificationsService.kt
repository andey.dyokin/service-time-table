package com.github.belbli.service

import com.github.belbli.model.Notification
import com.github.belbli.repository.NotificationRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Service

@Service
class NotificationsService @Autowired constructor(
    private val personInfoService: PersonInfoService,
    private val messagingTemplate: SimpMessagingTemplate,
    private val notificationRepository: NotificationRepository
) {

    fun sendNotification(notification: Notification) {
        messagingTemplate.convertAndSendToUser(
            notification.recipientId.toString(), "/queue/notifications",
            notification
        )
    }

    fun findUserNotifications(name: String): List<Notification> {
        val personInfo = personInfoService.findPersonInfoByEmailWithException(name)

        return notificationRepository.findAllByRecipientId(personInfo.personInfoId!!)
    }

    fun saveNotification(notification: Notification): Notification {
        return notificationRepository.save(notification)
    }
}