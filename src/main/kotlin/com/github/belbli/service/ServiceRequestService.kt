package com.github.belbli.service

import com.github.belbli.model.Client
import com.github.belbli.model.RequestStatus
import com.github.belbli.model.RootUser
import com.github.belbli.model.ServiceRequest
import com.github.belbli.model.Specialist
import com.github.belbli.model.UserRole
import com.github.belbli.model.requests.RequestStatusUpdate
import com.github.belbli.model.requests.ServiceRequestRequest
import com.github.belbli.repository.ServiceRequestRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.security.Principal
import java.util.*

@Service
class ServiceRequestService @Autowired constructor(
    private val serviceRequestRepository: ServiceRequestRepository,
    private val clientService: ClientService,
    private val specialistService: SpecialistService,
    private val personInfoService: PersonInfoService,
    private val organizationService: OrganizationService
) {

    private val rolesAllowedToUpdate = arrayOf(UserRole.ROOT_USER, UserRole.SPECIALIST)

    fun findServiceRequestById(serviceRequestId: UUID): ServiceRequest {
        return serviceRequestRepository.findByIdOrNull(serviceRequestId)
            ?: throw ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "service request with id = $serviceRequestId not found"
            )
    }

    fun createNewServiceRequest(serviceRequest: ServiceRequestRequest): ServiceRequest {
        val client = clientService.findClientById(serviceRequest.customerId)
        val specialist = specialistService.findSpecialist(serviceRequest.executorId)

        return serviceRequestRepository.save(
            ServiceRequest(
                requestId = null,
                customer = client,
                executor = specialist,
                subject = serviceRequest.subject,
                comment = serviceRequest.comment,
                status = RequestStatus.PENDING,
                serviceRequestComments = mutableListOf()
            )
        )
    }

    fun findAllSpecialistRequests(specialistId: UUID): List<ServiceRequest> {
        return serviceRequestRepository.findAllByExecutorSpecialistId(specialistId)
    }

    fun findAllClientRequests(clientId: UUID): List<ServiceRequest> {
        return serviceRequestRepository.findAllByCustomerClientId(clientId)
    }

    fun findAllOrganizationRequest(email: String, pageable: Pageable): Page<ServiceRequest> {
        val user = personInfoService.findUserByEmail(email)
        val organizationId = when (user) {
            is Specialist -> {
                user.organization?.organizationId
            }
            is RootUser -> {
                user.organization?.organizationId
            }
            is Client -> {
                return clientService.findClientRequests(user.clientId!!, pageable)
            }
            else -> {
                throw ResponseStatusException(HttpStatus.UNAUTHORIZED, "user is unauthorized to perform operation.")
            }
        }
        if (organizationId != null) {
            return organizationService.findOrganizationRequests(organizationId, pageable)
        } else {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "user is not a member of organization")
        }
    }

    fun updateServiceRequest(serviceRequest: ServiceRequest): ServiceRequest {
        return serviceRequestRepository.save(serviceRequest)
    }

    fun updateServiceRequestStatus(serviceRequestId: UUID, statusUpdate: RequestStatusUpdate, principal: Principal): ServiceRequest {
        val serviceRequestToUpdate = findServiceRequestById(serviceRequestId)
        val user = personInfoService.findPersonInfoByEmail(principal.name)
        if (serviceRequestToUpdate.executor.personInfo.email == principal.name || rolesAllowedToUpdate.contains(user?.userRole)) {
            return serviceRequestRepository.save(
                serviceRequestToUpdate.copy(
                    status = statusUpdate.newStatus
                )
            )
        }
        throw ResponseStatusException(HttpStatus.FORBIDDEN, "User is not authorized to perform the action")
    }
}