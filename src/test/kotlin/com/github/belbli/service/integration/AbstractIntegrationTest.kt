package com.github.belbli.service.integration

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.github.belbli.mapper.RootUserMapper
import com.github.belbli.model.Address
import com.github.belbli.model.Client
import com.github.belbli.model.Meeting
import com.github.belbli.model.Organization
import com.github.belbli.model.PersonInfo
import com.github.belbli.model.RootUser
import com.github.belbli.model.Specialist
import com.github.belbli.model.requests.RootUserAccountRequest
import com.github.belbli.service.RootUserService
import org.mockito.Mock
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.core.io.Resource
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests
import org.testng.Assert
import org.testng.annotations.BeforeClass
import java.io.FileReader
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertNotNull


@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.MOCK
)
@AutoConfigureTestDatabase
abstract class AbstractIntegrationTest : AbstractTransactionalTestNGSpringContextTests() {
    @Autowired
    protected lateinit var rootUserMapper: RootUserMapper

    @Autowired
    protected lateinit var rootUserService: RootUserService

    @Value("classpath:requests/web/root-user-correct-account-request.json")
    protected lateinit var correctRootUserRequestRes: Resource

    @Value("classpath:requests/service/correct-organization-request.json")
    protected lateinit var correctOrganizationRequest: Resource

    @Value("classpath:requests/service/correct-client-request.json")
    protected lateinit var correctClientRequest: Resource

    @Value("classpath:requests/service/correct-meeting-request.json")
    protected lateinit var correctMeetingRequest: Resource

    protected lateinit var rootUser: RootUser
    private lateinit var rootUserAccountRequest: RootUserAccountRequest

    @Mock
    protected lateinit var authentication: Authentication
    @MockBean
    protected lateinit var securityContext: SecurityContext

    val mapper = ObjectMapper().apply {
        registerModule(KotlinModule())
        configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    }

    @BeforeClass(alwaysRun = true)
    fun globalSetup() {
        rootUserAccountRequest = mapper.readValue(
            FileReader(correctRootUserRequestRes.file).readText(), RootUserAccountRequest::class.java
        )
        val rootUserMapped = rootUserMapper.toRootUser(rootUserAccountRequest)
        rootUser = rootUserService.createRootUser(
            rootUserMapped.copy(
                personInfo = rootUserMapped.personInfo.copy(
                    email = "globalRootUserForServiceTest${UUID.randomUUID()}@email.com"
                )
            )
        )
        authentication = Mockito.mock(Authentication::class.java)
        securityContext = Mockito.mock(SecurityContext::class.java)
        Mockito.`when`(securityContext.authentication).thenReturn(authentication)
        Mockito.`when`(authentication.name).thenReturn(rootUser.personInfo.email)
        SecurityContextHolder.setContext(securityContext)
    }

    protected fun assertEqualsOrganization(actual: Organization, expected: Organization) {
        assertNotNull(actual.organizationId)

        assertEquals(actual.name, expected.name)
        assertEquals(actual.description, expected.description)
        assertEqualsSpecialists(actual.specialists[0], expected.specialists[0])
        assertEqualsAddresses(actual.addresses[0], expected.addresses[0])
    }

    protected fun assertEqualsSpecialists(actual: Specialist, expected: Specialist) {
        assertNotNull(actual.specialistId)

        assertEquals(actual.experienceInYears, expected.experienceInYears)
        assertEquals(actual.role, expected.role)
        assertEqualsAddresses(actual.workAddress, expected.workAddress)
        assertEqualsPersonInfo(actual.personInfo, expected.personInfo)
    }

    protected fun assertEqualsPersonInfo(actual: PersonInfo, expected: PersonInfo) {
        assertNotNull(actual.personInfoId)

        assertEquals(actual.image, expected.image)
        assertEquals(actual.email, expected.email)
        assertEquals(actual.lastname, expected.lastname)
        assertEquals(actual.firstname, expected.firstname)
        //should be hashed after saving
        assertNotEquals(actual.password, expected.password)
    }

    protected fun assertEqualsAddresses(actual: Address, expected: Address) {
        assertNotNull(actual.addressId)

        assertEquals(actual.building, expected.building)
        assertEquals(actual.city, expected.city)
        assertEquals(actual.country, expected.country)
        assertEquals(actual.office, expected.office)
        assertEquals(actual.street, expected.street)
    }

    protected fun assertEqualsMeeting(actual: Meeting, expected: Meeting) {
        assertNotNull(actual.meetingId)

        Assert.assertEquals(actual.meetingId, expected.meetingId)
        Assert.assertEquals(actual.address, expected.address)
        Assert.assertEquals(actual.startDate.time, expected.startDate.time)
        Assert.assertEquals(actual.endDate.time, expected.endDate.time)
        Assert.assertEquals(actual.status, expected.status)
        Assert.assertEquals(actual.timeTableId, expected.timeTableId)
        Assert.assertEquals(actual.organizer.specialistId, expected.organizer.specialistId)
        Assert.assertEquals(actual.participants?.map { it.clientId }, expected.participants?.map { it.clientId })
    }

    protected fun assertEqualsClient(actual: Client, expected: Client) {
        assertNotNull(actual.clientId)

        assertEqualsPersonInfo(actual.personInfo, expected.personInfo)
        if (actual.meetings.isNotEmpty() && expected.meetings.isNotEmpty()) {
            assertEqualsMeeting(actual.meetings[0], expected.meetings[0])
        }
    }
}