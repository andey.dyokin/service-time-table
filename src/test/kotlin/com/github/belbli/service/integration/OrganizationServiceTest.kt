package com.github.belbli.service.integration

import com.github.belbli.mapper.OrganizationMapper
import com.github.belbli.model.requests.OrganizationWithSpecialistPasswordDto
import com.github.belbli.service.OrganizationService
import org.springframework.beans.factory.annotation.Autowired
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test
import java.io.FileReader


class OrganizationServiceTest : AbstractIntegrationTest() {
    @Autowired
    private lateinit var organizationMapper: OrganizationMapper

    @Autowired
    private lateinit var organizationService: OrganizationService

    private lateinit var correctRequest: OrganizationWithSpecialistPasswordDto

    @BeforeClass
    private fun init() {
        correctRequest = mapper.readValue(
            FileReader(correctOrganizationRequest.file).readText(),
            OrganizationWithSpecialistPasswordDto::class.java
        )
    }

    @Test(enabled = false)
    fun `test organization creation`() {
        val organizationRequest = organizationMapper.toOrganization(correctRequest)
        val organization = organizationService.createOrganization(
            organizationRequest
        )
        assertEqualsOrganization(organization, organizationRequest)
    }

}