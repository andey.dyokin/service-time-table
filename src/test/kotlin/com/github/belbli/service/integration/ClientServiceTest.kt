package com.github.belbli.service.integration

import com.github.belbli.mapper.ClientMapper
import com.github.belbli.model.PersonInfo
import com.github.belbli.model.requests.ClientDtoWithPassword
import com.github.belbli.repository.PersonInfoRepository
import com.github.belbli.service.ClientService
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.core.io.Resource
import org.springframework.web.server.ResponseStatusException
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test
import java.io.FileReader
import java.util.*

class ClientServiceTest : AbstractIntegrationTest() {
    @Autowired
    private lateinit var clientMapper: ClientMapper

    @Autowired
    private lateinit var clientService: ClientService

    @MockBean
    @Autowired
    private lateinit var personInfoRepository: PersonInfoRepository

    private lateinit var clientDto: ClientDtoWithPassword

    @BeforeClass
    private fun init() {
        clientDto = mapper.readValue(FileReader(correctClientRequest.file).readText(), ClientDtoWithPassword::class.java)
    }

    @Test
    fun testCreateClient() {
        val client = clientMapper.toClient(clientDto)
        val createdClient = clientService.createClient(client)

        assertEqualsClient(createdClient, client)
    }

    @Test(expectedExceptions = [ResponseStatusException::class])
    fun testCreateClientWithExistingEmail() {
        Mockito.`when`(personInfoRepository.findByEmail(clientDto.personInfo.email))
                .thenReturn(PersonInfo(
                        UUID.randomUUID(), "name",
                        "lastname", "secret",
                        "user@email.com", null)
                )

        val client = clientMapper.toClient(clientDto)
        clientService.createClient(client)
    }
}