package com.github.belbli.service.integration

import com.github.belbli.mapper.ClientMapper
import com.github.belbli.mapper.OrganizationMapper
import com.github.belbli.model.dto.MeetingDto
import com.github.belbli.model.requests.ClientDtoWithPassword
import com.github.belbli.model.requests.NewMeetingRequest
import com.github.belbli.model.requests.OrganizationWithSpecialistPasswordDto
import com.github.belbli.service.ClientService
import com.github.belbli.service.OrganizationService
import com.github.belbli.service.TimetableService
import org.springframework.beans.factory.annotation.Autowired
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test
import java.io.FileReader
import java.util.*

@Test(enabled = false)
class TimetableServiceTest : AbstractIntegrationTest() {

    @Autowired
    private lateinit var clientMapper: ClientMapper

    @Autowired
    private lateinit var organizationMapper: OrganizationMapper

    @Autowired
    private lateinit var clientService: ClientService

    @Autowired
    private lateinit var timetableService: TimetableService

    @Autowired
    private lateinit var organizationService: OrganizationService

    private lateinit var clientDto: ClientDtoWithPassword
    private lateinit var meetingDto: MeetingDto
    private lateinit var organizationDto: OrganizationWithSpecialistPasswordDto

    @BeforeClass
    private fun init() {
        clientDto =
            mapper.readValue(FileReader(correctClientRequest.file).readText(), ClientDtoWithPassword::class.java)
        meetingDto = mapper.readValue(FileReader(correctMeetingRequest.file).readText(), MeetingDto::class.java)
        organizationDto = mapper.readValue(
            FileReader(correctOrganizationRequest.file).readText(),
            OrganizationWithSpecialistPasswordDto::class.java
        )
    }

    @Test(enabled = false)
    fun testAddMeeting() {
        val organizationRequest = organizationMapper.toOrganization(organizationDto)
        val organization = organizationService.createOrganization(
            organizationRequest
        )
        val clientRequest = clientMapper.toClient(clientDto)
        val client = clientService.createClient(clientRequest)
        val organizer = organization.specialists[0]

        val newMeeting = timetableService.addMeeting(
            NewMeetingRequest(
                organizerId = organizer.specialistId!!,
                participantEmails = listOf(client.personInfo.email),
                address = organizationDto.addresses!![0],
                startDate = Date(Date().time + 86400),
                endDate = Date(Date().time + 90000)
            )
        )

        val clientById = clientService.findClientById(client.clientId!!)
        val timetableById = timetableService.getTimetableById(organizer.timeTable?.timeTableId!!)

        assertEqualsMeeting(newMeeting, clientById.meetings[0])
        assertEqualsMeeting(newMeeting, timetableById.meetings[0])
        assertEqualsOrganization(organization, organizationRequest)
        assertEqualsClient(client, clientRequest)
    }


}