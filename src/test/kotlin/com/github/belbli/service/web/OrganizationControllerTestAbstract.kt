package com.github.belbli.service.web

import com.github.belbli.model.requests.OrganizationWithSpecialistPasswordDto
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test
import java.io.FileReader
import java.util.*

@Test(enabled = false)
class OrganizationControllerTestAbstract : AbstractWebIntegrationTest() {
    @Value("classpath:requests/service/correct-organization-request.json")
    private lateinit var correctOrganizationRequest: Resource

    private lateinit var correctRequest: OrganizationWithSpecialistPasswordDto

    @BeforeClass
    fun setup() {
        correctRequest = mapper.readValue(
            FileReader(correctOrganizationRequest.file).readText(),
            OrganizationWithSpecialistPasswordDto::class.java
        ).copy(
            rootUserId = UUID.fromString(rootUserId)
        )
    }

    @Test(enabled = false)
    fun testCreateOrganizationIfNotAuthorized() {
        this.mvc.perform(
            MockMvcRequestBuilders.post("/api/v1/organizations/new")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(correctRequest))
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(MockMvcResultMatchers.status().`is`(HttpStatus.UNAUTHORIZED.value()))
            .andExpect(MockMvcResultMatchers.header().doesNotExist(HttpHeaders.AUTHORIZATION))
    }

    @Test(enabled = false)
    @WithMockUser("user")
    fun testCreateOrganizationIfAuthorized() {
        this.mvc.perform(
            MockMvcRequestBuilders.post("/api/v1/organizations/new")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, rootUserToken)
                .content(mapper.writeValueAsString(correctRequest))
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.header().doesNotExist(HttpHeaders.AUTHORIZATION))
    }
}