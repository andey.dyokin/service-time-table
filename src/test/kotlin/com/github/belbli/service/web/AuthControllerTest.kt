package com.github.belbli.service.web

import com.github.belbli.model.requests.ClientDtoWithPassword
import com.github.belbli.model.requests.RootUserAccountRequest
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.header
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.testng.annotations.BeforeClass
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import java.io.FileReader

class AuthControllerTest : AbstractWebIntegrationTest() {
    @Value("classpath:requests/web/client-correct-account-request.json")
    private lateinit var correctClientRequestRes: Resource

    @Value("classpath:requests/web/root-user-correct-account-request.json")
    private lateinit var correctRootUserRequestRes: Resource

    private lateinit var clientAccountRequest: ClientDtoWithPassword
    private lateinit var rootUserAccountRequest: RootUserAccountRequest

    @BeforeClass
    fun setup() {
        clientAccountRequest = mapper.readValue(
            FileReader(correctClientRequestRes.file).readText(), ClientDtoWithPassword::class.java
        )
        rootUserAccountRequest = mapper.readValue(
            FileReader(correctRootUserRequestRes.file).readText(), RootUserAccountRequest::class.java
        )
    }

    @Test
    fun testRootUserAccountCreation() {
        this.mvc.perform(
            post("/api/v1/auth/new/organization-account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(rootUserAccountRequest))
                .accept(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isOk)
        .andExpect(header().exists(HttpHeaders.AUTHORIZATION))
    }

    @Test(dataProvider = "badRootAccountRequestPayloads")
    fun testRootUserAccountCreationWithBadEmail(request: RootUserAccountRequest) {
        this.mvc.perform(
            post("/api/v1/auth/new/organization-account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request))
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isBadRequest)
            .andExpect(header().doesNotExist(HttpHeaders.AUTHORIZATION))
    }

    @Test
    fun testClientAccountCreation() {
        this.mvc.perform(
            post("/api/v1/auth/signup/client")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(clientAccountRequest))
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk)
            .andExpect(header().exists(HttpHeaders.AUTHORIZATION))
    }

    @Test(dataProvider = "badClientRequestPayloads")
    fun testClientAccountCreationWithBadEmail(request: ClientDtoWithPassword) {
        this.mvc.perform(
            post("/api/v1/auth/signup/client")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request))
                .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isBadRequest)
            .andExpect(header().doesNotExist(HttpHeaders.AUTHORIZATION))
    }

    @DataProvider(name = "badRootAccountRequestPayloads")
    private fun badRootAccountRequestPayloadsProvider(): Array<Array<RootUserAccountRequest>> {
        return arrayOf(
            arrayOf(
                rootUserAccountRequest.copy(
                    personInfo = rootUserAccountRequest.personInfo.copy(
                        email = "bad@com"
                    )
                )
            ),
            arrayOf(
                rootUserAccountRequest.copy(
                    personInfo = rootUserAccountRequest.personInfo.copy(
                        image = "not-an-url"
                    )
                )
            ),
            arrayOf(
                rootUserAccountRequest.copy(
                    personInfo = rootUserAccountRequest.personInfo.copy(
                        firstname = ""
                    )
                )
            ),
            arrayOf(
                rootUserAccountRequest.copy(
                    personInfo = rootUserAccountRequest.personInfo.copy(
                        lastname = ""
                    )
                )
            ),
            arrayOf(
                rootUserAccountRequest.copy(
                    personInfo = rootUserAccountRequest.personInfo.copy(
                        password = "tooSmal"
                    )
                )
            ),
        )
    }

    @DataProvider(name = "badClientRequestPayloads")
    private fun badClientRequestPayloadsProvider(): Array<Array<ClientDtoWithPassword>> {
        return arrayOf(
            arrayOf(
                clientAccountRequest.copy(
                    personInfo = clientAccountRequest.personInfo.copy(
                        email = "bad@com"
                    )
                )
            ),
            arrayOf(
                clientAccountRequest.copy(
                    personInfo = rootUserAccountRequest.personInfo.copy(
                        image = "not-an-url"
                    )
                )
            ),
            arrayOf(
                clientAccountRequest.copy(
                    personInfo = rootUserAccountRequest.personInfo.copy(
                        firstname = ""
                    )
                )
            ),
            arrayOf(
                clientAccountRequest.copy(
                    personInfo = rootUserAccountRequest.personInfo.copy(
                        lastname = ""
                    )
                )
            ),
            arrayOf(
                clientAccountRequest.copy(
                    personInfo = rootUserAccountRequest.personInfo.copy(
                        password = "tooSmal"
                    )
                )
            ),
        )
    }
}