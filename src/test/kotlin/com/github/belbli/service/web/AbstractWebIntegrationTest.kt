package com.github.belbli.service.web

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import com.github.belbli.model.dto.RootUserDto
import com.github.belbli.model.requests.RootUserAccountRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import org.testng.annotations.BeforeClass
import java.io.FileReader
import java.util.*


@SpringBootTest
@AutoConfigureTestDatabase
class AbstractWebIntegrationTest : AbstractTransactionalTestNGSpringContextTests() {
    protected lateinit var rootUserId: String
    protected lateinit var rootUserToken: String

    @Value("classpath:requests/web/root-user-correct-account-request.json")
    private lateinit var correctRootUserRequestRes: Resource
    private lateinit var rootUserAccountRequest: RootUserAccountRequest

    protected val mapper = ObjectMapper().apply {
        registerModule(KotlinModule())
        configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    }

    @Autowired
    private lateinit var wac: WebApplicationContext

    protected lateinit var mvc: MockMvc

    @BeforeClass(alwaysRun = true)
    fun globalSetup() {
        mvc = MockMvcBuilders.webAppContextSetup(wac)
            .apply<DefaultMockMvcBuilder>(springSecurity())
            .build()

        rootUserAccountRequest = mapper.readValue(
            FileReader(correctRootUserRequestRes.file).readText(), RootUserAccountRequest::class.java
        )
        rootUserToken = registerRootUser()

    }

    protected fun registerRootUser(): String {
        val rootUserResponse = this.mvc.perform(
            MockMvcRequestBuilders.post("/api/v1/auth/new/organization-account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    mapper.writeValueAsString(
                        rootUserAccountRequest.copy(
                            personInfo = rootUserAccountRequest.personInfo.copy(
                                email = "globalRootUserForWebTest${UUID.randomUUID()}@email.com"
                            )
                        )
                    )
                )
                .accept(MediaType.APPLICATION_JSON)
        ).andReturn()
        rootUserId = mapper.readValue<RootUserDto>(rootUserResponse.response.contentAsString).rootUserId.toString()
        return rootUserResponse.response.getHeaderValue(HttpHeaders.AUTHORIZATION).toString()
    }
}